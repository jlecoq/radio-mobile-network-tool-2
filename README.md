# Radio Mobile Network Tool

# Project-E: Radio propagation model - Epstein

BAMFO Kwabena  
LECOQ Julien  
NAGY Victor  
RAMOUSSET Vincent  
    
    The first thing to do is to download 
    the ENTIRE repository by cloning it (radio-mobile-network-tool-2)!

HOW TO LAUNCH THE APPLICATION
------------------------------

In the project repository (radio-mobile-network-tool-2):
- Replace the mapbox API token in config/mapbox.js.
- Open the file index.html with a browser (Safari, Firefox, Chrome, Brave, ...).

HOW TO RUN EPSTEIN PROPAGATION MODEL (QUICKSTART)
-------------------------------------------------

At anytime you can follow the instructions on the top.

    Initialization:
        - Left click on the map, a zone is selected.

    In the zone, you can add antennas and buildings.

    Add antennas:
        - In the bottom menu bar, click on the antenna icon (4th icon).
        - Left click in the blue zone.

    Add buildings:
        Add a building corner:
        - In the bottom menu bar, click on the building icon (3rd icon).
        - Left click on the map to add the corner.
        - [Repeat the previous steps 2..N times].
        Add the last building corner:        
        - DOUBLE CLICK to add the last corner.
        A popup appears:
            - Indicate the building height (10 meters by default).

    Run the Epstein model:
        - On the left side menu, under "Propagation model",
        select the "Epstein Method".
        - Then, left click on the first icon, the calculations are launched.
        This step should be really quick.
        - To finish, left click on "Power mapping" to print the
        heatmap.
        - This step could take some times depending on the zone size,
        by default it should be no more than 1 minutes.

Pre-configuration files are also available to save you from having to configure everything yourself by clicking on the "load a configuration file" button.
Pre-configuration files are in the "inputData" folder at the root of the project repository. 

TROUBLESHOOTING
---------------

In case of errors:
- reload the page (F5)
- restart the browser

On some browser, an uncommon configuration might not allow to run properly
the simulation.

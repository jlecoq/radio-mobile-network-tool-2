class CellMapItem extends MapListItem {
    constructor() {
        super();
    }

    whenSelected() {
        this.bin.empty();

        const seuil = parseInt(document.getElementById('seuil').value);
        let a = 0;

        for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
            for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                let max = -1000;
                let bestant = -1;

                for (a = 0; a < Antenna.nbAntennas; a++) {
                    if (Propagation.powers[a][i][j] > max) {
                        max = Propagation.powers[a][i][j];
                        bestant = a;
                    }
                }

                if (max < seuil) {
                    continue;
                }

                const plat = this.zone.toLat(i);
                const plng = this.zone.toLng(j);   

                let r = (bestant * 82) % 213;
                let v = (bestant * 17) % 207;
                let b = (bestant * 87) % 107 + 107;

                let strRed, strGreen, strBlue;

                if (r < 16) {
                    strRed = "0" + r.toString(16);
                } else {
                    strRed = r.toString(16);
                }

                if (v < 16) {
                    strGreen = "0" + v.toString(16);
                } else {
                    strGreen = v.toString(16);
                }

                if (b < 16) {
                    strBlue = "0" + b.toString(16);
                } else {
                    strBlue = b.toString(16);
                }

                const couleur = "#" + strRed + strGreen + strBlue;

                const locations = [
                    [plat, plng],
                    [plat + this.zone.latBin, plng],
                    [plat + this.zone.latBin, plng + this.zone.lngBin],
                    [plat, plng + this.zone.lngBin]
                ];

                const options = {
                    fillColor: couleur,
                    fillOpacity: 1,
                    color: 'transparent'
                };

                const bin = this.map.addPolygon(locations, options);

                this.bin.add(bin);
            }
        }
    } 
}
class DiffractionLossMapItem extends MapListItem {
    constructor() {
        super();
    }

    whenSelected() {
        this.bin.empty();

        const seuil = -1000;

        for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
            for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                let min = 1000;
                
                for (let a = 0; a < Antenna.nbAntennas; a++) {                    
                    if (Propagation.diffractionLoss[a][i][j] < min) {
                        min = Propagation.diffractionLoss[a][i][j];
                    }
                }

                // No value has been computed for this point because of a step different greater than 1.
                if (min === seuil) {
                    continue;
                }
            
                const plat = this.zone.toLat(i);
                const plng = this.zone.toLng(j);                
                                
                const couleur = ColorUtil.diffractionLossColor(Math.floor(min));

                const locations = [
                    [plat, plng],
                    [plat + this.zone.latBin, plng],
                    [plat + this.zone.latBin, plng + this.zone.lngBin],
                    [plat, plng + this.zone.lngBin]
                ];

                const options = {
                    fillColor: couleur,
                    fillOpacity: 1,
                    weight: 0,
                    color: 'none'
                };

                const bin = this.map.addPolygon(locations, options);        
                
                this.bin.add(bin);                
            }
        }
    } 
}
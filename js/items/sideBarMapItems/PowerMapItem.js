class PowerMapItem extends MapListItem {
    constructor() {
        super();
    }

    whenSelected() {
        this.bin.empty();

        const seuil = parseInt(document.getElementById('seuil').value);

        for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
            for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                let max = -1000;

                // Keep the biggest signal value.
                for (let a = 0; a < Antenna.nbAntennas; a++) {
                    if (Propagation.powers[a][i][j] > max) {
                        max = Propagation.powers[a][i][j];
                    }
                }

                if (max < seuil) {
                    continue;
                }

                const plat = this.zone.toLat(i);
                const plng = this.zone.toLng(j);                
                                                                
                const couleur = ColorUtil.GetColor(Math.floor(max));
                // const couleur = ColorUtil.GetColor(Math.floor(Propagation.powers[1][i][j]));                                      
                
                const locations = [
                    [plat, plng],
                    [plat + this.zone.latBin, plng],
                    [plat + this.zone.latBin, plng + this.zone.lngBin],
                    [plat, plng + this.zone.lngBin]
                ];

                const options = {
                    fillColor: couleur,
                    fillOpacity: 1,
                    weight: 0,
                    color: 'none'
                };

                const bin = this.map.addPolygon(locations, options);        
                
                this.bin.add(bin);                
            }
        }
    }
}
class InterferenceMapItem extends MapListItem {
    constructor() {
        super();
    }

    whenSelected() {
        this.bin.empty();

        for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
            for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                let max = -1;
                let suminterf = Math.pow(10, -15);

                for (let a = 0; a < Antenna.nbAntennas; a++) {
                    if (Math.pow(10, Propagation.powers[a][i][j] / 10.0) > max) {
                        if (max !== -1) {
                            suminterf += max;
                        }
                        max = Math.pow(10, Propagation.powers[a][i][j] / 10.0);
                    } else {
                        suminterf += Math.pow(10, Propagation.powers[a][i][j] / 10.0);
                    }
                }

                let CIR = max / suminterf;
                let r, g, b;

                if (CIR > 1.5) {
                    continue;
                } else if (CIR > 1) {
                    r = 1;
                    g = 69;
                    b = 250;
                } else if (CIR > 0.5) {
                    r = 1;
                    g = 250;
                    b = 57;
                } else if (CIR > 0.33) {
                    r = 188;
                    g = 251;
                    b = 0;
                } else if (CIR > 0.25) {
                    r = 244;
                    g = 251;
                    b = 0;
                } else if (CIR > 0.20) {
                    r = 251;
                    g = 220;
                    b = 0;
                } else {
                    r = 250;
                    g = 123;
                    b = 0;
                }

                let strRed, strGreen, strBlue;

                if (r < 16) {
                    strRed = "0" + r.toString(16);
                } else {
                    strRed = r.toString(16);
                }

                if (g < 16) {
                    strGreen = "0" + g.toString(16);
                } else {
                    strGreen = g.toString(16);
                }

                if (b < 16) {
                    strBlue = "0" + b.toString(16);
                } else {
                    strBlue = b.toString(16);
                }

                const couleur = "#" + strRed + strGreen + strBlue;

                const plat = this.zone.toLat(i);
                const plng = this.zone.toLng(j);   

                const locations = [
                    [plat, plng],
                    [plat + this.zone.latBin, plng],
                    [plat + this.zone.latBin, plng + this.zone.lngBin],
                    [plat, plng + this.zone.lngBin]
                ];

                const options = {
                    fillColor: couleur,
                    fillOpacity: 1,
                    color: 'transparent'                    
                };

                const bin = this.map.addPolygon(locations, options);                

                this.bin.add(bin);
            }
        }
    } 
}
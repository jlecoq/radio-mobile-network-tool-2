class MapListItem extends Item {
    constructor() {
        super();

        const options = {
            subscribeOnce: true
        };

        this.eventService.subscribe(EVENT.INITIALIZED_BIN, initizalizedBin => this.bin = initizalizedBin, options);
    }

    choixenv() {
        const m = document.getElementById('methode').value;

        if (m === "8") {
            const rep = confirm("Est un environnement urbain ?");

            if (rep === true) {
                Propagation.CCost = 3;
            } else {
                Propagation.CCost = 0;
            }
        }

        document.getElementById('footer').getItemById('propag').disabled = false;
        document.getElementById('side-bar-map-list').disableItems();
    }
}
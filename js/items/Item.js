class Item {
    constructor() {
        this.eventService = new EventService();

        const options = {
            subscribeOnce: true
        };
        
        this.eventService.subscribe(EVENT.INITIALIZED_MAP, initizalizedMap => this.map = initizalizedMap, options);
        this.eventService.subscribe(EVENT.PLACED_ZONE, initizalizedZone => this.zone = initizalizedZone, options);
    }

    whenSelected() {
    }  
}
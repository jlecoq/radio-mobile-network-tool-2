class PropagationItem extends CustomFooterItem {
    constructor() {
        super();

        if (!PropagationItem.sharedInstance) {
            PropagationItem.sharedInstance = this;
        }

        return PropagationItem.sharedInstance;
    }

    // Action when the radio propagation computing is required: click on the button.
    whenSelected() {
        // Avoid re-click on the button during the computation.
        document.getElementById('footer').getItemById('propag').disabled = true;

        // Normalize the zone.elevation data.
        if (Propagation.firstPropagation === true) {
            Propagation.firstPropagation = false;
            let min = 10000;

            for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
                for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                    if (this.zone.relief[i][j] < min) {
                        min = this.zone.relief[i][j];
                    }
                }
            }

            for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
                for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                    this.zone.relief[i][j] = this.zone.relief[i][j] - min;
                }
            }
        }

        for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
            for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                this.zone.elevation[i][j] = this.zone.relief[i][j] + this.zone.architecture[i][j];
            }
        }

        Propagation.powers = [];

        for (let a = 0; a < Antenna.nbAntennas; a++) {
            Propagation.powers.push([]);
            Propagation.diffractionLoss.push([]);

            for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
                Propagation.powers[a].push([]);
                Propagation.diffractionLoss[a].push([]);

                for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                    Propagation.powers[a][i].push([]);
                    Propagation.powers[a][i][j] = -1000;
                    Propagation.diffractionLoss[a][i].push([]);
                    Propagation.diffractionLoss[a][i][j] = -1000;
                }
            }
        }

        document.getElementById('side-bar-map-list').disableItems();

        switch (document.getElementById('methode').value) {
            case "1":
                this.propagationSKE();
                break;
            case "2":
                this.propagationBull();
                break;
            case "3":
                // this.propagationEpsteinPeterson();
                this.propagationEpsteinPeterson2();                
                break;
            case "8":
                this.propagationCostHata();
                break;
            case "9":
                // this.propagationWalfisch();
                break;
            default:
                break;
        }

        document.getElementById('side-bar-map-list').activeItems();
    }

    propagationCostHata() {
        let bande = parseInt(document.getElementById('bande').value);
        let cst1 = 1.56 * Math.log10(bande) + 0.8;
        let cst2 = 46.3 + 33.9 * Math.log10(bande);

        let tilt;
        let azimut;
        let jant;
        let iinf;
        let jsup;
        let jinf;
        let iant;
        let isup;
        let ipoint;
        let jpoint;
        let d;
        let Loss;
        let ahr;
        let hant;
        let antenna;
        let point;

        for (let a = 0; a < Antenna.nbAntennas; a++) {
            antenna = Antenna.antennas[a].location;
            point = L.latLng(antenna.lat, antenna.lng);
            tilt = Antenna.antennas[a].tilt;
            tilt = 180 * tilt / Math.PI;

            if (Antenna.antennas[a].isOmni === false) {
                azimut = Antenna.antennas[a].azimut;
                azimut = 180 * azimut / Math.PI;
            }

            iant = this.zone.toI(antenna.lat);
            jant = this.zone.toJ(antenna.lng);
            hant = this.zone.elevation[iant][jant] + 10;

            if (-200 < -iant) {
                iinf = -iant;
            } else {
                iinf = -200;
            }

            if (-200 < -jant) {
                jinf = -jant;
            } else {
                jinf = -200;
            }

            if (200 > this.zone.numberOfBinsInLat - iant) {
                isup = this.zone.numberOfBinsInLat - iant;
            } else {
                isup = 200;
            }

            if (200 > this.zone.numberOfBinsInLng - jant) {
                jsup = this.zone.numberOfBinsInLng - jant;
            } else {
                jsup = 200;
            }

            for (let jdist = jinf; jdist <= jsup; jdist++)
                for (let idist = iinf; idist <= isup; idist++) {
                    ipoint = iant + idist; // Coordonnées en maille d'un point de reception
                    jpoint = jant + jdist;

                    if (ipoint < 0 || jpoint < 0 || ipoint >= this.zone.numberOfBinsInLat || jpoint >= this.zone.numberOfBinsInLng) {
                        continue;
                    }

                    point.lat = antenna.lat + idist * this.zone.latBin
                    point.lng = antenna.lng + jdist * this.zone.lngBin;
                    d = L.GeometryUtil.length([antenna, point]);

                    if (idist === 0 && jdist === 0) { // Si c'est la position de l'antenne alors ignorer
                        Loss = 0;
                    } else {
                        let hpoint = this.zone.elevation[ipoint][jpoint] + 1.5;

                        if (Propagation.CCost === 3) {
                            ahr = 3.2 * (Math.log10(11.75 * hpoint)) * (Math.log10(11.75 * hpoint)) - 4.97;
                        } else {
                            ahr = (1.1 * Math.log10(bande) - 0.7) * hpoint - cst1;
                        }

                        Loss = cst2 - 13.82 * Math.log10(hant);
                        Loss = Loss - ahr + (44.9 - 6.55 * Math.log10(hant)) * Math.log10(d / 1000.0) + Propagation.CCost;

                        let angle = 180 * Math.atan(Math.abs(hant - hpoint) / d) / Math.PI;

                        angle = Math.abs(tilt - angle);

                        let angleindex = Math.floor(angle / 5);
                        Loss = Loss + Propagation.CCost - Propagation.VertRadiationPattern[angleindex];

                        if (Antenna.antennas[a].isOmni === false) {
                            if (idist === 0 && jdist > 0) {
                                angle = 90;
                            } else if (idist === 0 && jdist < 0) {
                                angle = 270;
                            } else {
                                angle = 180 * Math.atan(jdist / idist) / Math.PI;

                                if (idist < 0) {
                                    angle = angle + 180;
                                }

                                if (angle < 0) {
                                    angle = angle + 360;
                                }
                            }

                            angle = Math.abs(angle - azimut);
                            angleindex = Math.floor(angle / 5);
                            Loss = Loss - Propagation.HorzRadiationPattern[angleindex];
                        }
                    }
                    Propagation.powers[a][ipoint][jpoint] = Antenna.antennas[a].power - Loss;
                }
        }
    }

    propagationSKE() {
        let tilt;
        let azimut;
        let jant;
        let iinf;
        let jsup;
        let jinf;
        let iant;
        let isup;
        let ipoint;
        let jpoint;
        let d;
        let Loss;
        let distPasBin;
        let hv;
        let dobs;
        let hmax;
        let hvmax;
        let hant;
        let R1;
        let v;
        let jpas;
        let delta;
        let hpoint;
        let point;
        let ipas;

        for (let a = 0; a < Antenna.nbAntennas; a++) {
            let antenna = Antenna.antennas[a].location;
            point = L.latLng(antenna.lat, antenna.lng);
            iant = this.zone.toI(antenna.lat);
            jant = this.zone.toJ(antenna.lng)
            hant = this.zone.elevation[iant][jant] + 10;
            let bande = parseInt(document.getElementById('bande').value);

            // TODO: why?
            tilt = Antenna.antennas[a].tilt;
            tilt = 180 * tilt / Math.PI;

            // TODO: why?
            if (Antenna.antennas[a].isOmni === false) {
                azimut = Antenna.antennas[a].azimut;
                azimut = 180 * azimut / Math.PI;
                console.log('ok');                
            }

            if (-200 < -iant) {
                iinf = -iant;
            } else {
                iinf = -200;
            }

            if (-200 < -jant) {
                jinf = -jant;
            } else {
                jinf = -200;
            }

            if (200 > this.zone.numberOfBinsInLat - iant) {
                isup = this.zone.numberOfBinsInLat - iant;
            } else {
                isup = 200;
            }

            if (200 > this.zone.numberOfBinsInLng - jant) {
                jsup = this.zone.numberOfBinsInLng - jant;
            } else {
                jsup = 200;
            }

            for (var jdist = jinf; jdist <= jsup; jdist++) {
                for (var idist = iinf; idist <= isup; idist++) {
                    ipoint = iant + idist; // Coordonnées en maille d'un point de reception.
                    jpoint = jant + jdist;

                    if (ipoint < 0 || jpoint < 0 || ipoint >= this.zone.numberOfBinsInLat || jpoint >= this.zone.numberOfBinsInLng) {
                        continue;
                    }

                    point.lat = antenna.lat + idist * this.zone.latBin
                    point.lng = antenna.lng + jdist * this.zone.lngBin;

                    // TODO: To finish.
                    this.map.addMarker(point);
                    d = GeoUtil.distance(antenna, point);

                    if (idist === 0 && jdist === 0) { // Si c'est la position de l'antenne alors ignorer
                        Loss = 32.4 + 20 * Math.log10((Math.sqrt(this.zone.binWidth * this.zone.binWidth + this.zone.binHeight * this.zone.binHeight) / 2) / bande) + 20 * Math.log10(bande);
                    } else {
                        hpoint = this.zone.elevation[ipoint][jpoint];
                        delta = hant - hpoint - 1.5;

                        let alpha; // L'angle fait entre l'antenne, les latitudes et le point de reception
                        if (jdist !== 0) {
                            alpha = Math.atan(idist / (jdist));

                            if (idist < 0 && jdist < 0) {
                                alpha = alpha + 3.14;
                            } else if (idist >= 0 && jdist < 0) {
                                alpha = alpha + 3.14;
                            }
                        } else {
                            if (idist > 0) {
                                alpha = 1.5707;
                            } else {
                                alpha = -1.5707;
                            }
                        }

                        jpas = Math.cos(alpha); // Pas d'avance sur la ligne directe entre l'antenne et le point de reception
                        ipas = Math.sin(alpha);
                        distPasBin = Math.sqrt(ipas * ipas * this.zone.binHeight * this.zone.binHeight + jpas * jpas * this.zone.binWidth * this.zone.binWidth);
                        let iobstacle = Math.floor(iant + ipas); // Premier obstacle
                        let jobstacle = Math.floor(jant + jpas);

                        hmax = this.zone.elevation[iobstacle][jobstacle]; // Hauteur du 1er obstacle
                        dobs = distPasBin;
                        hvmax = hant - (dobs / d) * delta;
                        let sobstacle = 1;

                        for (let s = 1; Math.abs(s * ipas) < Math.abs(idist); s++) { // Parcours des obstacle avec un pas ipas, jpas
                            dobs = s * distPasBin;
                            hv = hant - (dobs / d) * delta;

                            if (this.zone.elevation[Math.floor(iant + s * ipas)][Math.floor(jant + s * jpas)] - hv > hmax - hvmax) { // Si c'est l'obstacle le plus haut.
                                hmax = this.zone.elevation[Math.floor(iant + s * ipas)][Math.floor(jant + s * jpas)]; // Alors le stocker.
                                hvmax = hv;
                                iobstacle = Math.floor(iant + s * ipas);
                                jobstacle = Math.floor(jant + s * jpas);
                                sobstacle = s;
                            }
                        }

                        let LKE = 0.0;
                        hmax = hmax - hvmax;

                        if (hmax > 0) {
                            let obstacle = L.latLng(antenna.lat + sobstacle * ipas * this.zone.latBin, antenna.lng + sobstacle * jpas * this.zone.lngBloc / this.zone.numberOfBinsOnBlockSide);
                            let d1 = GeoUtil.distance(antenna, obstacle);
                            let d2 = GeoUtil.distance(point, obstacle);
                            R1 = Math.sqrt(0.3 * d1 * d2 / (d1 + d2));
                            v = hmax * Math.sqrt(2) / R1;

                            if (v > 1) {
                                LKE = -20 * Math.log10(0.225 / v);
                            }
                        }

                        if (LKE < 0) {
                            LKE = 0.0;
                        }

                        let LFS = 32.4 + 20 * Math.log10((d) / 1000.0) + 20 * Math.log10(bande);

                        Loss = LKE + LFS;
                        let angle = 180 * Math.atan(Math.abs(hant - hpoint) / d) / Math.PI;

                        angle = tilt - angle;

                        if (angle < 0) {
                            angle = 360 + angle;
                        }

                        let angleindex = Math.floor(angle / 5);
                        Loss = Loss + Propagation.CCost - Propagation.VertRadiationPattern[angleindex];

                        if (Antenna.antennas[a].isOmni === false) {
                            if (idist === 0 && jdist > 0) {
                                angle = 90;
                            } else if (idist === 0 && jdist < 0) {
                                angle = 270;
                            } else {
                                angle = 180 * Math.atan(jdist / idist) / Math.PI;
                                if (idist < 0) {
                                    angle = angle + 180;
                                }
                                if (angle < 0) {
                                    angle = angle + 360;
                                }
                            }

                            angle = Math.abs(angle - azimut);
                            angleindex = Math.floor(angle / 5);
                            Loss = Loss - Propagation.HorzRadiationPattern[angleindex];
                        }
                    }
                    Propagation.powers[a][ipoint][jpoint] = Antenna.antennas[a].power - Loss;
                }
            }
        }
    }

    propagationBull() {
        // TODO: Somee errors inside this function.
        let bande = parseInt(document.getElementById('bande').value);

        let dobs;
        let dobs1;
        let dobs2;
        let hant;
        let R1;
        let v;
        let jpas;
        let iant;
        let delta;
        let ipoint;
        let hpoint;
        let jant;
        let tilt;
        let jpoint;
        let iinf;
        let jinf;
        let isup;
        let jsup;
        let hv;
        let hobs;
        let sobs;
        let delta1;
        let delta2;
        let point;
        let d1, d2;

        for (let a = 0; a < Antenna.nbAntennas; a++) {
            let antenna = Antenna.antennas[a].location;
            point = L.latLng(antenna.lat, antenna.lng);
            iant = Math.floor((antenna.lat - this.zone.southWestCorner.lat) / this.zone.latBin);
            jant = Math.floor((antenna.lng - this.zone.southWestCorner.lng) / this.zone.lngBin);
            hant = this.zone.elevation[iant][jant] + 10;
            tilt = Antenna.antennas[a].tilt;
            if (-200 < -iant) {
                iinf = -iant;
            } else {
                iinf = -200;
            }
            if (-200 < -jant) {
                jinf = -jant;
            } else {
                jinf = -200;
            }
            if (200 > this.zone.numberOfBinsInLat - iant) {
                isup = this.zone.numberOfBinsInLat - iant;
            } else isup = 200;
            if (200 > this.zone.numberOfBinsInLng - jant) {
                jsup = this.zone.numberOfBinsInLng - jant;
            } else {
                jsup = 200;
            }

            for (var jdist = jinf; jdist <= jsup; jdist++)
                for (var idist = iinf; idist <= isup; idist++) {
                    if (idist == 0 && jdist === 0) {
                        continue; //si c'est la position de l'antenne alors ignorer
                    }
                    ipoint = iant + idist; //coordonnées en maille d'un point de reception
                    jpoint = jant + jdist;

                    if (ipoint < 0 || jpoint < 0 || ipoint >= this.zone.numberOfBinsInLat || jpoint >= this.zone.numberOfBinsInLng) {
                        continue;
                    }

                    point.lat = antenna.lat + idist * this.zone.latBin
                    point.lng = antenna.lng + jdist * this.zone.lngBin;
                    let d = L.GeometryUtil.length([antenna, point]);

                    hpoint = this.zone.elevation[ipoint][jpoint];
                    delta = hant - hpoint - 1.5;

                    let alpha; // L'angle fait entre l'antenne, les latitudes et le point de reception

                    if (jdist !== 0) {
                        alpha = Math.atan(idist / (jdist));
                        if (idist < 0 && jdist < 0) {
                            alpha = alpha + 3.14;
                        } else if (idist >= 0 && jdist < 0) {
                            alpha = alpha + 3.14;
                        }
                    } else {
                        if (idist > 0) {
                            alpha = 1.5707;
                        } else {
                            alpha = -1.5707;
                        }
                    }

                    jpas = Math.cos(alpha); //pas d'avance sur la ligne directe entre l'antenne et le point de reception
                    let ipas = Math.sin(alpha);
                    let distPasBin = Math.sqrt(ipas * ipas * this.zone.binHeight * this.zone.binHeight + jpas * jpas * this.zone.binWidth * this.zone.binWidth);
                    let stotal = Math.floor(d / distPasBin);
                    let s1 = 0;
                    let tangmax = 0;
                    for (let s = 1; Math.abs(s * ipas) < Math.abs(idist) && Math.abs(s * jpas) < Math.abs(jdist); s++) { //parcours des obstacle avec un pas ipas, jpas
                        dobs = s * distPasBin;
                        hv = hant - (dobs / d) * delta;
                        hobs = this.zone.elevation[Math.floor(iant + s * ipas)][Math.floor(jant + s * jpas)]
                        if (hobs > hv && ((hobs - hant) / dobs) > tangmax) { //si c'est l'obstacle le plus haut
                            tangmax = (hobs - hant) / dobs;
                            s1 = s;
                        }
                    }

                    let s2 = 0;
                    tangmax = 0;
                    for (let s = 1; Math.abs(s * ipas) < Math.abs(idist); s++) { //parcours des obstacle avec un pas ipas, jpas
                        dobs = s * distPasBin;
                        hv = hpoint + 1.5 - (dobs / d) * delta;
                        hobs = this.zone.elevation[Math.floor(ipoint - s * ipas)][Math.floor(jpoint - s * jpas)]
                        if (hobs > hv && ((hobs - hpoint) / dobs) > tangmax) { //si c'est l'obstacle le plus haut
                            tangmax = (hobs - hpoint) / dobs;
                            s2 = s;
                        }
                    }
                    var LBull = 0.0;
                    sobs = 0;
                    hobs = 0;
                    if (s1 == 0 && s2 > 0) {
                        sobs = stotal - s2;
                        hobs = this.zone.elevation[Math.floor(ipoint - s2 * ipas)][Math.floor(jpoint - s2 * jpas)];
                    }
                    if (s1 > 0 && s2 == 0) {
                        sobs = s1;
                        hobs = this.zone.elevation[Math.floor(iant + s1 * ipas)][Math.floor(jant + s1 * jpas)];
                    }
                    if (s1 > 0 && s2 > 0) {
                        delta1 = (hant - this.zone.elevation[Math.floor(iant + s1 * ipas)][Math.floor(jant + s1 * jpas)]);
                        delta2 = (hpoint + 1.5 - this.zone.elevation[Math.floor(ipoint - s2 * ipas)][Math.floor(jpoint - s2 * jpas)]);
                        d1 = s1 * distPasBin;
                        d2 = s2 * distPasBin;
                        const s3 = stotal - s2;
                        let min = 1000.0;
                        for (let s = s1; s <= s3; s++) {
                            dobs1 = s * distPasBin;
                            dobs2 = (stotal - s) * distPasBin;

                            if (Math.abs((hpoint + 1.5 - delta2 * dobs2 / d2) - (hant - delta1 * dobs1 / d1)) < min) {
                                min = Math.abs((hpoint + 1.5 - delta2 * dobs2 / d2) - (hant - delta1 * dobs1 / d1));
                                sobs = s;
                                hobs = hpoint + 1.5 - delta2 * dobs2 / d2;
                            }
                        }
                    }
                    if (s1 === 0 && s2 === 0) {
                        LBull = 0.0;
                    } else {
                        let obstacle = new google.maps.LatLng(antenna.lat + sobs * ipas * this.zone.latBin, antenna.lng + sobs * jpas * this.zone.lngBin);
                        d1 = google.maps.geometry.spherical.computeDistanceBetween(antenna, obstacle);
                        d2 = google.maps.geometry.spherical.computeDistanceBetween(point, obstacle);
                        R1 = Math.sqrt(0.3 * d1 * d2 / (d1 + d2));
                        hv = hant - (d1 / (d1 + d2)) * delta;
                        v = (hobs - hv) * Math.sqrt(2) / R1;
                        if (v > 1) LBull = -20 * Math.log10(0.225 / v);
                    }
                    if (LBull < 0) {
                        if (LBull < 0) LBull = 0.0;
                    }
                    let LFS = 32.4 + 20 * Math.log10((d) / 1000.0) + 20 * Math.log10(bande);

                    let L = LFS + LBull
                    let angle = 180 * Math.atan(Math.abs(hant - hpoint) / d) / Math.PI;
                    angle = tilt - angle;
                    if (angle < 0) angle = 360 + angle;
                    let angleindex = Math.floor(angle / 5);
                    L = L + Propagation.CCost - Propagation.VertRadiationPattern[angleindex];

                    if (Antenna.antennas[a].isOmni == false) {
                        if (idist == 0 && jdist > 0) {
                            angle = 90;
                        } else if (idist == 0 && jdist < 0) {
                            angle = 270;
                        } else {
                            angle = 180 * Math.atan(jdist / idist) / Math.PI;
                            if (idist < 0) {
                                angle = angle + 180;
                            }
                            if (angle < 0) {
                                angle = angle + 360;
                            }
                        }
                        angle = Math.abs(angle - azimut);
                        angleindex = Math.floor(angle / 5);
                        L = L - Propagation.HorzRadiationPattern[angleindex];
                    }

                    Propagation.powers[a][ipoint][jpoint] = Antenna.antennas[a].power - L;
                }
        }
    }

    propagationEpsteinPeterson() {
        let average_h = 0.5; // Input average mobile height.

        // Input the operating wavelength.
        const wavelength = GeoUtil.CELERITY / (document.getElementById('bande').value * 10 ** 6);

        // Initialize counters.
        let a; // Current index of the antenna.
        let i = 0;
        let j = 0;
        let k = 0;
        let m = 0;

        let profilePath = [];

        // Object data.
        let Obj_loc = [];
        let Obj_h = [];

        // Obstacle data.
        let Obst_h = [];
        let Obst_loc = [];

        let K = 0; // Number of objects in Profile Path.
        let M = 0; // Number of obstacles in Profile Path.

        // Temporary variables.
        let A = 0;
        let B = 0;
        let C = 0;
        let D = 0;
        let E = 0;
        let F = 0;

        let latlngs;
        let pointInPath;
        let i_pointInPath;
        let j_pointInPath;
        let objects;
        let objectIDs;

        let object_reference;
        let i_object;
        let j_object;

        let i_ms; // Position in i of the current checked point.
        let j_ms; // Position in j of the current checked point.
        let ms_h = 0; // Average mobile height plus the elevation on 3D map.
        let ms_loc; 

        let step = 2; // step to jump between each bins.
        let Loss = 0;
        let h_ec = 0;

        let point;

        let antenna; // Current antenna location (lat, lng).
        let cell_loc = 0; // Cell location in meters (represented as distance from antenna to antenna).
        let cell_h = 0; // Antenna height above the ground plus the elevation.
        let iant; // Positon in i of the antenna.
        let jant; // Position in j of the antenna.            

        let iinf;
        let jinf;

        let isup;
        let jsup;

        let jdist;
        let idist;

        let ipoint; // Position in i of the current checked point.
        let jpoint; // Position in j of the current checked point.

        let d; // Distance between the antenna and the current point in meters.
        let d1; // Distance between the antenna and the obstacle in meters.
        let d2; // Distance between the current checked point and the obstacle in meters.

        let h;

        // For every antenna.
        for (a = 0; a < Antenna.antennas.length; a++) {
            antenna = Antenna.antennas[a].location;
            point = L.latLng(antenna.lat, antenna.lng);

            // Position of the antenna in i and j relative to the zone.
            iant = this.zone.toI(antenna.lat);
            jant = this.zone.toJ(antenna.lng);
                             
            // Height of the antenna.            
            cell_h = this.zone.elevation[iant][jant] + Antenna.antennas[a].height;

            if (-200 < -iant) {
                iinf = -iant;
            } else {
                iinf = -200;
            }
            if (-200 < -jant) {
                jinf = -jant;
            } else {
                jinf = -200;
            }
            if (200 > this.zone.numberOfBinsInLat - iant) {
                isup = this.zone.numberOfBinsInLat - iant;
            } else {
                isup = 200;
            };
            if (200 > this.zone.numberOfBinsInLng - jant) {
                jsup = this.zone.numberOfBinsInLng - jant;
            } else {
                jsup = 200;
            };

            // Browse the zone.
            for (jdist = jinf; jdist <= jsup; jdist += step)
                for (idist = iinf; idist <= isup; idist += step) {
                    Loss = 0
                    ipoint = iant + idist;
                    jpoint = jant + jdist;                                        

                    // Check if the current point isn't outside the zone.
                    if (ipoint < 0 || jpoint < 0 || ipoint >= this.zone.numberOfBinsInLat || jpoint >= this.zone.numberOfBinsInLng) {                                                
                        continue
                    };

                    // TODO: Should we use this instead?
                    // point.lat = this.zone.toLat(ipoint);
                    // point.lng = this.zone.toLng(jpoint);

                    point.lat = antenna.lat + idist * this.zone.latBin; // TODO: antenna.lat should be this.southWestCorner.lat instead?
                    point.lng = antenna.lng + jdist * this.zone.lngBin; // TODO: antenna.lng should be this.southWestCorner.lng instead?
                    
                    // Distance between antenna and the current point in meters.
                    d = GeoUtil.distance(antenna, point);
                
                    if (idist !== 0 && jdist !== 0) { // If it's not the position of the antenna, we do the loss diffraction calculation.
                        // Section (1) from flowchart
                        // Get location of ms(i,j)
                        ms_loc = d;

                        // Extract path profile PP(i,j)
                        latlngs = [antenna, point];
                        profilePath = [];

                        // Browse all the points between the antenna and the current checked point to
                        // find all the points where a building is.
                        for (i = 0.05; i < 1; i += 0.05) {
                            pointInPath = L.GeometryUtil.interpolateOnLine(this.map, latlngs, i);

                            i_pointInPath = this.zone.toI(pointInPath.latLng.lat);
                            j_pointInPath = this.zone.toJ(pointInPath.latLng.lng);

                            if (this.zone.architecture[i_pointInPath][j_pointInPath] !== 0) {
                                profilePath.push(pointInPath);
                            }
                        }

                        // Extract list of objects from points, so as to
                        // ensure there are no duplicate objects.
                        objects = [];
                        objectIDs = [];
                        for (i = 0; i < Building.buildingPolygons.length; i++) {
                            for (j = 0; j < profilePath.length; j++) {
                                if (GeoUtil.isPointInsidePolygon(profilePath[j].latLng, Building.buildingPolygons[i]) &&
                                    !objectIDs.includes(Building.buildingPolygons[i]._leaflet_id)) {
                                    objects.push(Building.buildingPolygons[i]);
                                    objectIDs.push(Building.buildingPolygons[i]._leaflet_id);
                                }
                            }
                        }

                        K = objects.length;

                        // Get the Obj_loc(k) and Obj_h(k)
                        object_reference = [];                                                
                        for (k = 0; k < K; k++) {
                            object_reference = objects[k].getCenter();
                            Obj_loc[k] = GeoUtil.distance(antenna, object_reference);
                            i_object = this.zone.toI(object_reference.lat);
                            j_object = this.zone.toJ(object_reference.lng);
                            Obj_h[k] = this.zone.elevation[i_object][j_object];
                        }

                        // Section (4) from flowchart.
                        M = 0;                        
                        for (k = 0; k < K; k++) {                            
                            if (k === 0) {
                                A = cell_h;
                                B = cell_loc
                            }                                        

                            // Convert to I and J.
                            i_ms = this.zone.toI(point.lat);
                            j_ms = this.zone.toJ(point.lng);

                            // Get the height of the current checked point.
                            ms_h = average_h + this.zone.elevation[i_ms][j_ms];
                            
                            // Determine if an object is an obstacle.
                            if (Obj_h[k] + h_ec >= (((ms_h - A) / (ms_loc - B)) * B + A)) {
                                Obst_h[M] = Obj_h[k];
                                Obst_loc[M] = Obj_loc[k];

                                // Store the obstacle as a new source of line of sight.
                                A = Obj_h[k]; 
                                B = Obj_loc[k]; 
                                 
                                M += 1; // Increase the total number of obstacles.
                            }
                        }

                        // Section (2)
                        // If ms(i,j) is not in LOS, it means at least one obstacle is present, we calculate the diffraction loss.
                        if (M !== 0) { 
                            // Section (6) from flowchart.                      
                            for (m = 0; m < M; m++) {
                                if (m === 0) {
                                    C = cell_h;
                                    D = cell_loc;
                                } else {
                                    C = Obst_h[m - 1];
                                    D = Obst_loc[m - 1];
                                }

                                if (m === M - 1) {
                                    E = ms_h;
                                    F = ms_loc;
                                } else {
                                    E = Obst_h[m + 1];
                                    F = Obst_loc[m + 1];
                                }

                                // Section (8) from flowchart.
                                d1 = Math.abs(Obst_loc[m] - D); // Distance between the antenna and the obstacle in meters.
                                d2 = Math.abs(F - Obst_loc[m]); // Distance between the current checked point and the obstacle in meters.

                                // TODO: What should be the value of "r_e"?
                                // var r_e = 1;                                       
                                // h = Obst_h[m] - (C + ((E - C) * (Obst_loc[m] - D))) / (F - D) + (d1 * d2 / 2 * r_e);;
                                h = Obst_h[m]; // Obstacle height in meters.

                                // Compute the current diffraction loss.
                                Loss = PropagationUtil.diffractionLoss(Loss, h, wavelength, d1, d2);
                            }
                        }
                    }

                    // Update the loss and the power received on a cell by an antenna.
                    Propagation.powers[a][ipoint][jpoint] = Antenna.antennas[a].power + Loss;
                    Propagation.diffractionLoss[a][ipoint][jpoint] = Loss;
                }
        }

        console.log("Epstein peterson propagation model completed!");
    }

    propagationEpsteinPeterson2() {
        let average_h = 0.5; // Input average mobile height.

        // Input the operating wavelength.
        const wavelength = GeoUtil.CELERITY / (document.getElementById('bande').value * 10 ** 6);

        // Initialize counters.
        let a; // Current index of the antenna.
        let i = 0;
        let j = 0;
        let k = 0;
        let m = 0;

        // Temporary variables.
        let A = 0;
        let B = 0;
        let C = 0;
        let D = 0;
        let E = 0;
        let F = 0;

        let profilePath = []; // The profile path containing a list of points which are inside buildings.
        let pointInPath; // A point in profile path.
        let latlngs; 

        let objects; // The objects in profile path.
        let obstacles; // The obstacles in profile path. 
        let M = 0; // Number of obstacles in profile path.

        let object_reference;

        let step = 2; // step to jump between each bins.
        let loss = 0; // The diffraction loss.
        let h_ec = 0; // ?    

        let antenna; // Current antenna location (lat, lng, i, j).
        let cell_loc = 0; // Antenna location in meters (represented as distance from antenna to antenna).
        let cell_h = 0; // Antenna height above the ground plus the elevation.                

        let point = new Location(0, 0); // Position of the current checked point. // Average mobile height plus the elevation on 3D map.
 
        let d1; // Distance between the antenna and the obstacle in meters.
        let d2; // Distance between the current checked point and the obstacle in meters.

        let h; // Obstacle height in meters.
        
        // For every antenna.
        for (a = 0; a < Antenna.antennas.length; a++) {
            antenna = Antenna.antennas[a].location; 
            this.zone.setIJ(antenna); // Position of the antenna in i and j relative to the zone.
                                                 
            // Height of the antenna.            
            cell_h = this.zone.elevation[antenna.i][antenna.j] + Antenna.antennas[a].height;

            // Browse the zone.
            for (point.j = 0; point.j < this.zone.numberOfBinsInLng; point.j += step)
                for (point.i = 0; point.i < this.zone.numberOfBinsInLng; point.i += step) {                
                    loss = 0;                                                                    
                    this.zone.setLatLng(point, point.i, point.j);
                    
                    if (point.i !== antenna.i && point.j !== antenna.j) { // If it's not the position of the antenna, we do the loss diffraction calculation.
                        // Section (1) from flowchart
                        // Get location of ms(i,j)                              
                        point.loc = GeoUtil.distance(antenna, point); // Distance between antenna and the current point in meters.

                        // Extract path profile PP(i,j)
                        latlngs = [antenna, point];
                        profilePath = [];

                        // Browse all the points between the antenna and the current checked point to
                        // find all the points where a building is.
                        for (i = 0.05; i < 1; i += 0.05) {
                            pointInPath = L.GeometryUtil.interpolateOnLine(this.map, latlngs, i).latLng;                                                                                
                            this.zone.setIJ(pointInPath);                            

                            // this.map.addMarker(pointInPath);

                            if (this.zone.architecture[pointInPath.i][pointInPath.j] !== 0) {
                                profilePath.push(pointInPath);
                            }
                        }
                        // throw new Error();

                        // Extract list of objects from points, so as to
                        // ensure there are no duplicate objects.
                        objects = [];                        
                        for (i = 0; i < Building.buildingPolygons.length; i++) {
                            for (j = 0; j < profilePath.length; j++) {
                                if (GeoUtil.isPointInsidePolygon(profilePath[j], Building.buildingPolygons[i]) &&
                                    !objects.includes(Building.buildingPolygons[i])) {
                                    objects.push(Building.buildingPolygons[i]);                                    
                                }
                            }
                        }
                                                                     
                        // Get the Obj_loc(k) and Obj_h(k).
                        object_reference = [];                                                
                        for (k = 0; k < objects.length; k++) {
                            object_reference = objects[k].getCenter();
                            this.zone.setIJ(object_reference);                                                  
                            objects[k].loc = GeoUtil.distance(antenna, object_reference);
                            objects[k].h = this.zone.elevation[object_reference.i][object_reference.j];
                        }

                        // Section (4) from flowchart.
                        M = 0;  
                        obstacles = [];                         
                        // Loop through every objects and we stop if the number of obstacles is equals to 9.
                        // It's not insteresting to go further than 9 obstacles.
                        for (k = 0; k < objects.length && M < 9; k++) {                            
                            if (k === 0) {
                                A = cell_h;
                                B = cell_loc
                            }                                        

                            // Convert to I and J.
                            this.zone.setIJ(point);                            

                            // Get the height of the current checked point.
                            point.h = average_h + this.zone.elevation[point.i][point.j];
                                                        
                            // Determine if an object is an obstacle.                        
                            if (objects[k].h + h_ec >= (((point.h - A) / (point.loc - B)) * B + A)) { 
                                obstacles[M] = objects[k];                                

                                // Store the obstacle as a new source of line of sight.
                                A = objects[k].h; 
                                B = objects[k].loc; 
                                 
                                M += 1; // Increase the total number of obstacles.
                            }
                        }                                

                        // Section (2)
                        // If ms(i,j) is not in LOS, it means at least one obstacle is present, we calculate the diffraction loss.
                        if (M !== 0) { 
                            // Section (6) from flowchart.                      
                            for (m = 0; m < M; m++) {
                                if (m === 0) {
                                    C = cell_h;
                                    D = cell_loc;
                                } else {
                                    C = obstacles[m - 1].h;
                                    D = obstacles[m - 1].loc;
                                }

                                if (m === M - 1) {
                                    E = point.h;
                                    F = point.loc;
                                } else {
                                    E = obstacles[m + 1].h;
                                    F = obstacles[m + 1].loc;
                                }

                                // Section (8) from flowchart.                                                                
                                d1 = Math.abs(obstacles[m].loc - D); // Distance between the antenna and the obstacle in meters.
                                d2 = Math.abs(F - obstacles[m].loc); // Distance between the current checked point and the obstacle in meters.

                                // TODO: What should be the value of "r_e"?
                                // var r_e = 1;                                       
                                // h = Obst_h[m] - (C + ((E - C) * (Obst_loc[m] - D))) / (F - D) + (d1 * d2 / 2 * r_e);;
                                h = obstacles[m].h; // Obstacle height in meters.

                                // Compute the current diffraction loss.
                                loss = PropagationUtil.diffractionLoss(loss, h, wavelength, d1, d2);
                            }
                        }
                    }

                    // Update the loss and the power received on a cell by an antenna.
                    Propagation.powers[a][point.i][point.j] = Antenna.antennas[a].power + loss;
                    Propagation.diffractionLoss[a][point.i][point.j] = loss;                    
                }
        }

        console.log("Epstein peterson propagation model completed!");
    }
}
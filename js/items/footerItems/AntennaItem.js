class AntennaItem extends CustomFooterItem {
    constructor() {
        super();

        if (!AntennaItem.sharedInstance) {
            AntennaItem.sharedInstance = this;
        }
         
        return AntennaItem.sharedInstance;
    }

    whenSelected() {        
        document.getElementById('header').setMessage('Position the antenna or Esc to cancel!');
        document.getElementById('frameAntenna').style.visibility = "visible";        
        document.getElementById('omni').checked = true;

        document.getElementById('side-bar-map-list').disableItems(); 
        document.getElementById('footer').getItemById('propag').disabled = true;
    }

    unselected() {
        super.unselected();
        document.getElementById('footer').getItemById('propag').disabled = false;
        document.getElementById('frameAntenna').style.visibility = 'hidden';
    }

    placeItem(location) {
        const antenna = new Antenna({location, omni: true});    
        const { PI, sin, cos } = Math;

        // If the antenna is added by a mouse click.
        if (typeof (type) === 'undefined') {
            antenna.isOmni = document.getElementById('omni').checked;
            antenna.power = parseInt(document.getElementById('power').value);
            antenna.tilt = parseInt(document.getElementById('tilt').value) * PI / 180; // TODO: Issue?
            antenna.transmissionFrequency = parseInt(document.getElementById('bande').value) * 1_000_000; // Convert MHz => to KHz.
            antenna.lambda = GeoUtil.CELERITY / antenna.transmissionFrequency;
            antenna.height = parseInt(document.getElementById('antenna-height').value);            
            
            // TODO: Issue? Different from the initialization in the propagations models.
            if (!antenna.isOmni) {
                antenna.azimut = parseInt(document.getElementById('azimut').value) * PI / 180;
            }
        } 

        // Display the antenna.
        // Circle if it is an omni antenna.
        if (antenna.isOmni === true) {
            const options = {
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 0.5,
                radius: 10
            };

            const circle = this.map.addCircle(location, options);

            // Add the circle for this city to the map.
            Antenna.markerAntennas.push(circle); // TODO: Why didn't push it also in the else case?
        } else { // A line if it is a directional antenna.                
            const options1 = {
                weight: 1,
                color: 'red'
            }

            const positions1 = [
                location,
                L.latLng(location.lat + 2 * this.zone.latBin * sin(PI / 2 - antenna.azimut), location.lng + 2 * this.zone.lngBin * cos(PI / 2 - antenna.azimut))
            ];

            this.map.addPolyline(positions1, options1);

            const options2 = {
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 1,
                radius: 2
            };

            this.map.addCircle(location, options2);
        }        
    }

    /**
     * Place an antenna from an input data file.     
     */
    placeItemFromInputFile(object) {
        const antenna = new Antenna(object);

        // Display the antenna.
        // Circle if it is an omni antenna.
        if (antenna.isOmni === true) {
            const options = {
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 0.5,
                radius: 10
            };

            const circle = this.map.addCircle(antenna.location, options);

            // Add the circle for this city to the map.
            Antenna.markerAntennas.push(circle); // TODO: Why didn't push it also in the else case?
        } else { // A line if it is a directional antenna.                
            const options1 = {
                weight: 1,
                color: 'red'
            }

            const positions1 = [
                antenna.location,
                L.latLng(antenna.location.lat + 2 * this.zone.latBin * sin(PI / 2 - antenna.azimut), antenna.location.lng + 2 * this.zone.lngBin * cos(PI / 2 - antenna.azimut))
            ];

            this.map.addPolyline(positions1, options1);

            const options2 = {
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 1,
                radius: 2
            };

            this.map.addCircle(antenna.location, options2);
        }        
    }
}
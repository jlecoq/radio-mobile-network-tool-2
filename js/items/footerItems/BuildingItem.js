class BuildingItem extends CustomFooterItem {
    constructor() {
        super();      
        
        if (!BuildingItem.sharedInstance) {
            BuildingItem.sharedInstance = this;

            this.cornerMarkers = [];
        }

        return BuildingItem.sharedInstance;
    }

    clearCornersMarkers() {
        this.cornerMarkers.forEach(markers => markers.remove());
    }

    whenSelected() {
        if (Building.buildingInConstruction === null) {            
            document.getElementById('header').setMessage('Place the first corner of the buildling.');
        }                     

        document.getElementById('side-bar-map-list').disableItems();
    }

    unselected() {
        super.unselected();
    }

    /**
     * Begins the construction of a building.     
     */
    placeItem(location) {
        // Initialize the new building, and disable all the icons of the footer.         
        if (Building.buildingInConstruction === null) {
            Building.buildingInConstruction = new Building();
                        
            document.getElementById('footer').disableItems();

            const footerOffset = document.getElementById('footer').offsetHeight;

            this.cancelButton = document.createElement('button');
            this.cancelButton.textContent = 'cancel the building construction';
            this.cancelButton.style.position = 'absolute';
            this.cancelButton.style.transform = 'translate(-50%, -50%)';
            this.cancelButton.style.top = footerOffset + 22 + 'px';
            this.cancelButton.style.left = '50%';
            this.cancelButton.style.zIndex = 2;      
            this.cancelButton.style.color = 'white';
            this.cancelButton.style.padding = '13px';   
            this.cancelButton.style.borderRadius = '10px';
            this.cancelButton.style.border = 'none';
            this.cancelButton.style.backgroundColor = 'Salmon';  
            this.cancelButton.style.boxShadow = '0 3px 8px 0 rgba(0, 0, 0, 0.2)';

            this.cancelButton.addEventListener('click', () => {                
                this.resetState();
            });
                                    
            document.body.appendChild(this.cancelButton);
        }        

        Building.buildingInConstruction.cornersLocation.push(location);
        
        document.getElementById('header').setMessage(`Place corner ${Building.buildingInConstruction.cornersLocation.length + 1} of the buildling.`);
        
        const redMarker = L.ExtraMarkers.icon({                        
            icon: 'fa-number',
            number: Building.buildingInConstruction.cornersLocation.length,            
            shape: 'star'            
        });
          
        const options = {
            icon: redMarker
        };  
                
        const cornerMarker = this.map.addMarker(location, options);        

        this.cornerMarkers.push(cornerMarker);
    }

    /**
     * Completes the construction of a building.     
     */
    placeItem2(location) {      
        if (Building.buildingInConstruction.cornersLocation.length < 2) {
            alert('The number of corners should be superior to 2.');            
            return false;
        }                

        // Set the height of the building.
        const height = parseInt(prompt("Enter the height of the building in meters.", "10"));
        
        // Check if the heigh is a valid unsigned number superior to 0.
        if (isNaN(height) || height <= 0) {
            alert('You should enter an unsigned number superior to 0.');
            return false;
        }
    
        // Initialize the new building to be build, perform a copy of the current building in construction.
        const newBuilding = Object.create(Building.buildingInConstruction);        

        // Set the height of the building. 
        newBuilding.height = height;
    
        // Append the last corner of the building.
        newBuilding.cornersLocation.push(location);

        // Creating the polygon corresponding to the building.        
        const buildingPolygon = this.map.addPolygon(newBuilding.cornersLocation, {
                fillColor: 'grey',
                fillOpacity: 1,
                color : 'transparent'
        });
                
        // Add the new building to the list of buildings.
        Building.buildingPolygons.push(buildingPolygon);
        
        let point;

        // Called when double click on the building.
        buildingPolygon.on('dblclick', () => {
            const response = confirm("Do you want to delete the building?");

            // If the building has to be deleted.
            if (response === true) {                
                for(let i = 0; i < this.zone.numberOfBinsInLat; i++) {
                    for(let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                        point = L.latLng(this.zone.toLat(i), this.zone.toLng(j));

                        if(buildingPolygon.getBounds().contains(point)) {
                            this.zone.architecture[i][j] = 0;
                        }
                    }
                }

                // Remove the building from the list of buildings.
                const index = Building.buildingPolygons.indexOf(buildingPolygon);
                Building.buildingPolygons.splice(index, 1);       
                
                // Remove the polygon of the building from the map.
                buildingPolygon.remove();
            }
        });

        // Called when right click on the building.
        buildingPolygon.on('contextmenu', () => {                              
            alert("Height of the building: " + newBuilding.height + " meters.");
        });

        // Browse the study zone and update the architecture.
        for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
            for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {                                                
                point = L.latLng(this.zone.toLat(i), this.zone.toLng(j));
                                
                // If the building is place at the current position.
                if(buildingPolygon.getBounds().contains(point)) {
                    this.zone.architecture[i][j] = newBuilding.height;
                }                                
            }
        }

        this.resetState();

        return true;
    }

    resetState() {
        // Remove the markers from the map.
        this.clearCornersMarkers();

        // Active the every footer item.
        document.getElementById('footer').activeItems();

        Building.buildingInConstruction = null;

        document.body.removeChild(this.cancelButton);

        document.getElementById('header').setDefaultMesssage();
    }

   /**
     * Place a building from an input data file.     
     */
    placeItemFromInputFile(building) {
        // Check if the heigh is a valid unsigned number superior to 0.
        if (isNaN(building.height) || building.height <= 0) {
            alert('You should enter an unsigned number superior to 0.');
            return;
        }

        // Creating the polygon corresponding to the building.        
        const buildingPolygon = this.map.addPolygon(building.cornersLocation, {
                fillColor: 'grey',
                fillOpacity: 1,
                color : 'transparent'
        });
                    
        // Add the new building to the list of buildings.
        Building.buildingPolygons.push(buildingPolygon);

        // Called when double click on the building.
        buildingPolygon.on('dblclick', () => {
            const response = confirm("Do you want to delete the building?");

            // If the building has to be deleted.
            if (response === true) {                
                for(let i = 0; i < this.zone.numberOfBinsInLat; i++) {
                    for(let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                        point = L.latLng(this.zone.toLat(i), this.zone.toLng(j));

                        if(buildingPolygon.getBounds().contains(point)) {
                            this.zone.architecture[i][j] = 0;
                        }
                    }
                }

                // Remove the building from the list of buildings.
                const index = Building.buildingPolygons.indexOf(buildingPolygon);
                Building.buildingPolygons.splice(index, 1);       
                
                // Remove the polygon of the building from the map.
                buildingPolygon.remove();
            }
        });


        // Called when right click on the building.
        buildingPolygon.on('contextmenu', () => {                              
            alert("Height of the building: " + building.height + " meters.");
        });

        let point;

        // Browse the study zone and update the architecture.
        for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
            for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {                                                
                point = L.latLng(this.zone.toLat(i), this.zone.toLng(j));
                                
                // If the building is place at the current position.
                if(buildingPolygon.getBounds().contains(point)) {
                    this.zone.architecture[i][j] = building.height;
                }                                
            }
        }
    }
}

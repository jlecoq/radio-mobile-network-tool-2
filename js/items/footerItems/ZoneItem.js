class ZoneItem extends CustomFooterItem {
    constructor() {
        super()

        if (!ZoneItem.sharedInstance) {
            ZoneItem.sharedInstance = this;
        }      

        return ZoneItem.sharedInstance;
    }

    // Action after the selection of the south-east corner of the covering area
    placeZone(location) {
        const numberOfBlocksInLng = parseInt(document.getElementById('largZone').value);
        const numberOfBlocksInLat = parseInt(document.getElementById('hautZone').value);

        new Zone(location, numberOfBlocksInLat, numberOfBlocksInLng);
        
        
        document.getElementById('header').setMessage("Scan in progress!!!");        
        document.getElementById('largZone').disabled = 'disabled';
        document.getElementById('hautZone').disabled = 'disabled';

        for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
            this.zone.relief.push([]);
            this.zone.elevation.push([]);
            this.zone.architecture.push([]);
            // this.zone.cellCoverage.push([]); // TODO: Not used, implement it.
            // this.zone.isOnRoad.push([]); // TODO: Not used, implement it.
            // this.zone.orientation.push([]); // TODO: Not used, implement it.
            for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                this.zone.relief[i].push(0);
                this.zone.elevation[i].push(0);
                this.zone.architecture[i].push(0);
                // this.zone.cellCoverage[i].push(-1); // TODO: Not used, implement it.
                // this.zone.isOnRoad[i].push([]); // TODO: Not used, implement it.
                // this.zone.isOnRoad[i][j] = false; // TODO: Not used, implement it.
                // this.zone.orientation[i].push([]); // TODO: Not used, implement it.
                // this.zone.orientation[i][j] = -1; // TODO: Not used, implement it.
            }
        }

        this.drawPath();
    }

    drawPath() {
        this.map.addLayer(Zone.group1);
        
        // Generation of random relief : waiting for leaflet API to get real data
        for (let ligne = 0; ligne < this.zone.numberOfBinsOnBlockSide; ligne++) { // For each line in the current block
            for (let col = 0; col < this.zone.numberOfBinsOnBlockSide; col++) { // For each column in the current block
                if (col === 0 && ligne === 0) {
                    this.zone.relief[Zone.bl * this.zone.numberOfBinsOnBlockSide + ligne][Zone.bc * this.zone.numberOfBinsOnBlockSide + col] = MathUtil.randomIntFrom0To(10);
                } else if (col === 0) {
                    this.zone.relief[Zone.bl * this.zone.numberOfBinsOnBlockSide + ligne][Zone.bc * this.zone.numberOfBinsOnBlockSide + col] = Math.abs(this.zone.relief[Zone.bl * this.zone.numberOfBinsOnBlockSide + ligne - 1][Zone.bc * this.zone.numberOfBinsOnBlockSide + col] + MathUtil.randomIntFrom0To(3) - 1);
                } else if (ligne === 0) {
                    this.zone.relief[Zone.bl * this.zone.numberOfBinsOnBlockSide + ligne][Zone.bc * this.zone.numberOfBinsOnBlockSide + col] = Math.abs(this.zone.relief[Zone.bl * this.zone.numberOfBinsOnBlockSide + ligne][Zone.bc * this.zone.numberOfBinsOnBlockSide + col - 1] + MathUtil.randomIntFrom0To(3) - 1);
                } else {
                    this.zone.relief[Zone.bl * this.zone.numberOfBinsOnBlockSide + ligne][Zone.bc * this.zone.numberOfBinsOnBlockSide + col] = Math.abs((this.zone.relief[Zone.bl * this.zone.numberOfBinsOnBlockSide + ligne - 1][Zone.bc * this.zone.numberOfBinsOnBlockSide + col] + this.zone.relief[Zone.bl * this.zone.numberOfBinsOnBlockSide + ligne - 1][Zone.bc * this.zone.numberOfBinsOnBlockSide + col]) / 2 + MathUtil.randomIntFrom0To(3) - 1);
                }
            }
        }
        
        // Display the scanned this.zone.block.
        let contourZone = L.polygon([
            [this.zone.bloc.lat, this.zone.bloc.lng],
            [this.zone.bloc.lat + this.zone.latBin * this.zone.numberOfBinsOnBlockSide, this.zone.bloc.lng],
            [this.zone.bloc.lat + this.zone.latBin * this.zone.numberOfBinsOnBlockSide, this.zone.bloc.lng + this.zone.lngBloc],
            [this.zone.bloc.lat, this.zone.bloc.lng + this.zone.lngBloc]
        ]);
                
        contourZone.addTo(Zone.group1);
        
        // TODO: block column and block line.
        Zone.bc++;
        
        if (Zone.bc === this.zone.numberOfBlocksInLng) {
            Zone.bc = 0;
            Zone.bl++;
            this.zone.bloc = L.latLng(this.zone.bloc.lat + this.zone.latBin * this.zone.numberOfBinsOnBlockSide, this.zone.southWestCorner.lng);
        } else {
            this.zone.bloc = L.latLng(this.zone.bloc.lat, this.zone.bloc.lng + this.zone.lngBloc);
        }

        // Fin du scan de la this.zone étudiée.
        if (Zone.bl === this.zone.numberOfBlocksInLat) {
            // Active footer items.
            const customFooter = document.getElementById('footer');
            customFooter.activeItems();

            this.map.removeLayer(Zone.group1);

            // Display the deployment area.
            contourZone = [
                [this.zone.southWestCorner.lat, this.zone.southWestCorner.lng],
                [this.zone.southWestCorner.lat + this.zone.latBloc * this.zone.numberOfBlocksInLat, this.zone.southWestCorner.lng],
                [this.zone.southWestCorner.lat + this.zone.latBloc * this.zone.numberOfBlocksInLat, this.zone.southWestCorner.lng + this.zone.lngBloc * this.zone.numberOfBlocksInLng],
                [this.zone.southWestCorner.lat, this.zone.southWestCorner.lng + this.zone.lngBloc * this.zone.numberOfBlocksInLng]
            ];

            this.map.addPolygon(contourZone);

            this.unselected();

            return;
        }
        
        // Scan the next block.
        setTimeout(() => this.drawPath(), 10);
    }

    /**
     * Place the zone from an input data file.     
     */
    placeItemFromInputFile(zone) {
        new Zone(zone.southWestCorner, zone.numberOfBlocksInLat, zone.numberOfBlocksInLng);        

        document.getElementById('header').setMessage("Scan in progress!!!");
        document.getElementById('largZone').disabled = 'disabled';
        document.getElementById('hautZone').disabled = 'disabled';

        for (let i = 0; i < this.zone.numberOfBinsInLat; i++) {
            this.zone.relief.push([]);
            this.zone.elevation.push([]);
            this.zone.architecture.push([]);
            for (let j = 0; j < this.zone.numberOfBinsInLng; j++) {
                this.zone.relief[i].push(0);
                this.zone.elevation[i].push(0);
                this.zone.architecture[i].push(0);
            }
        }
        
        this.drawPath();
    }
}
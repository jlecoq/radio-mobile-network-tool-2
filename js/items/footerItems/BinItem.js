class BinItem extends CustomFooterItem {
    constructor() {
        super();

        if (!BinItem.sharedInstance) {
            BinItem.sharedInstance = this;
        }
        
        this.bin = new Bin();

        return BinItem.sharedInstance;
    }

    whenSelected() {
        this.bin.empty();
    }    
}
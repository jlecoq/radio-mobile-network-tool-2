class LineOfSightItem extends CustomFooterItem {
    constructor() {
        super();
      
        if (!LineOfSightItem.sharedInstance) {
            LineOfSightItem.sharedInstance = this;
        }

        this.lineOfSight = null; 

        return LineOfSightItem.sharedInstance;
    }

    whenSelected() {
    }

    placeItem(location) {
        if (this.lineOfSight === null) {
            this.lineOfSight = new LineOfSight();
            this.lineOfSight.firstPosition = location;
        } else {
            this.lineOfSight.secondPosition = location;

            // Convert the leaflet LatLng object into a Location object.
            const firstPosition = LocationHelper.toLocation(this.lineOfSight.firstPosition);            
            const secondPosition = LocationHelper.toLocation(this.lineOfSight.secondPosition);

            const options = {
                step: 50
            };

            // Creating the list of positions of the segment.
            this.lineOfSight.positions = GeoUtil.pointsFromStraightLine(firstPosition, secondPosition, options);
                 
            // Add the markers to the map.
            this.map.addMarkers(this.lineOfSight.positions);
                        
            // Check for every building if it is crossed by the line of sight and log the result
            Building.buildingPolygons.forEach(buildingPolygon => {                
                const isCrossed = this.isLineOfSightBlockedByPolygon(this.lineOfSight, buildingPolygon);
                
                if (isCrossed) {
                    console.log('The building is crossed by the line of sight.');                                        
                }            
            });

            this.lineOfSight = null;
        }
    }

    isLineOfSightBlockedByPolygon(lineOfSight, polygon) {
        // Loop through every position on the line of sight.
        for (const position of lineOfSight.positions) { 
            if (GeoUtil.isPointInsidePolygon(position, polygon)) {                
                return true;
            }
        }
        
        return false;
    }
}
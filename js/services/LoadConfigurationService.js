class LoadConfigurationService {
    constructor() {
        if (!LoadConfigurationService.sharedInstance) {
            LoadConfigurationService.sharedInstance = this;
        }
                
        this.validType = ['Building'];

        this.getItemInstance = {
            'building': new BuildingItem(),
            'zone': new ZoneItem(),
            'antenna': new AntennaItem() 
        }

        return LoadConfigurationService.sharedInstance;
    }

    load(dataJson) {        
        let zonePlaced = false;

        // For very item place it.
        for (const item of dataJson) {
            if (typeof item !== "object") {
                console.error('The JSON file contains an invalid data.');
                return;
            }

            // Cueck if the zone has already been placed.
            if (item.type === 'zone') {

                if (zonePlaced) {
                    console.error('The zone has already be placed.');
                    return;
                } else {
                    zonePlaced = true;
                }                
            }

            // Get the instance of the item to place.
            const itemInstance = this.getItemInstance[item.type];            
        
            if (itemInstance === undefined) {
                console.error('The JSON file contains an invalid data.');
                return;
            }

            // Place the item.
            itemInstance.placeItemFromInputFile(item);                 
        }    
    }
    
}
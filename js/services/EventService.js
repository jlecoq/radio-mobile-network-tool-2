/**
 * @brief the event class, it allows you to emit event and subscribe to the published data by the event.
 * Example of use case: eventService.subscribe(Event.INITIALIZED_MAP, map => console.log(map));
 */
class EventService {
    constructor() {
        if (!EventService.sharedInstance) {
            EventService.sharedInstance = this;
        }
        
        this.events = {};    
        return EventService.sharedInstance;
    }

    /**
     * @brief subscribe to an event.
     * @param event the event to subscribe for.
     * @param handler the handler which will be called when the event is fired.
     */
    subscribe(event, handler, opts) {
        // Setting up the default options. 
        const options = {
            subscribeOnce: false,
            ...opts
        };
        
        // Creating the subscriber.
        let subscriber = {
            handler,
            options
        };
        
        const subscribers = this.getSubscribers(event);
        const index = subscribers.push(subscriber) - 1;

        const retainedData = this.getRetainedData(event);

        // Run once the subscriber handler if a retained data is available.
        if (retainedData) {
            subscriber.handler(retainedData);

            // Remove the subscriber from the list of subscribers.
            if (subscriber.options.subscribeOnce) {
                subscribers.splice(index, 1);
                subscriber = null;
            }
        }

        // Return an object allowing to unsubscribe.
        return {
            unsubscribe: function() {
                if (subscriber !== null) {
                    subscribers.splice(index, 1); 
                    subscriber = null;               
                }                  
            }
        }
    }

    /**
     * @brief publish to an event.
     * @param event the event to publish for.
     * @param data the transmitted data.
     * @param retain a flag to memorize or not the published data. If the flag is true, the data will be memorized and
     * send to every new subscribers which subscribe after the publishment.
     */
    publish(event, data, retain) {
        const oldSubscribers = this.getSubscribers(event);
        
        const currentSubscribers = oldSubscribers.filter(subscriber => {
            subscriber.handler(data);

            if (subscriber.options.subscribeOnce) {                                
                return false;
            }

            return true;
        });

        this.setSubscribers(event, currentSubscribers);

        if (retain) {
            this.setRetainedData(event, data);
        }
    }

    /**
     * @brief get the subscribers of an event.
     * @param event the event where we want the subscribers.
     * @returns the matching subscribers.
     */
    getSubscribers(event) {
        if (!this.events[event]) {
            this.events[event] = {
                subscribers: [],
                retainData: null
            }
        }
        
        return this.events[event].subscribers;
    }

    /**
     * @brief set the new subscribers list of an event.
     * @param event the event where we want to define the new subscribers.
     * @param subscribers the list of subscribers.
     */
    setSubscribers(event, subscribers) {
        this.events[event].subscribers = subscribers;
    };

    /**
     * @brief return the retained data of an event.
     * @param event the event of the retained data.
     * @returns the reatained data.
     */
    getRetainedData(event) {
        return this.events[event].retainData;
    }

    /**
     * @brief retain a data for an event.
     * @param event the event we want the retained data.
     * @param data the data to retain.
     */
    setRetainedData(event, data) {
        this.events[event].retainData = data;
    }
}

// The list of available event.
const EVENT = {
    INITIALIZED_MAP: 'INITIALIZED_MAP',
    PLACED_ZONE: 'PLACED_ZONE',
    INITIALIZED_BIN: 'INITIALIZED_BIN',
    PLACED_BUILDING: 'PLACED_BUILDING'
};
/**
 * @brief the footer of the main page.
 */
class CustomFooter extends HTMLElement {
    constructor() {
        super();

        this.shadow = this.attachShadow({ mode: 'open' });

        // Create main section.
        this.mainSection = document.createElement('section');
        this.mainSection.id = 'section-footer';

        // TODO: Some icons wasn't defined (teacher code). 
        // Creating the list of icons.
        const listIcons = {
            'propag': new PropagationItem(),
            'bin': new BinItem(),
            // 'routes': routes,
            // 'routes+': completer,
            // 'routes2': addaxe,
            'bat': new BuildingItem(),
            // 'saveNet': saveNet,
            // 'loadNet': loadNet,
            'antenna': new AntennaItem(),
            'straight_line': new LineOfSightItem()
            // 'ecole': addEcole
        }

        const pathToIcon = './icons/footer-icons/';

        // Add every items to the footer section.
        for (const [id, item] of Object.entries(listIcons)) {            
            const button = document.createElement('button');

            button.id = id;
            button.className = "footer-item";
            button.disabled = true;

            // Put the correct icon image for the button.
            button.style.backgroundImage = `url(${pathToIcon}${id}.png)`;

            // Event handler.
            button.addEventListener('click', () => this.setSelectedItem(item));                        
                                    
            // Append the button to the section.
            this.mainSection.appendChild(button);            
        }                        

        // Creating the file upload button.
        const input = document.createElement('input');
        input.type = 'file';
        input.id = 'load-file-configuration-input';
        input.accept = '.json';
        input.style.width = '180px';                
        input.style.height = '35px';                
        
        // Little trick to hide the input and show a button instead.
        const labelDiv = document.createElement('div');   
        labelDiv.className = 'load-file-configuration-button';            

        // labelDiv.innerText = 'load a configuration file';
        const textDiv = document.createElement('span');
        textDiv.textContent = 'load a configuration file';
        textDiv.id = 'load-file-configuration-text'; 

        // labelDiv.innerHTML = 'load a configuration file';
        labelDiv.appendChild(textDiv);
        labelDiv.appendChild(input);
        
        labelDiv.style.width = input.style.width;                        
        labelDiv.style.height = input.style.height;              
            
        // On selected file. 
        input.addEventListener('change', () => this.loadFileConfiguration(input));

        this.mainSection.appendChild(labelDiv);

        // CSS of the custom-footer element.
        this.shadow.innerHTML = `
            <style>    
                #section-footer {
                    width: 100%;
                    height: 100%;
                    display: flex;
                    justify-content: flex-start;
                    align-items: center;
                }
                
                #section-footer > * {
                    margin: 0 10px 0 10px;
                }
            
                .footer-item {                          
                    width: 42px;
                    height: 42px;   
                    background-position: center;
                    background-repeat: no-repeat;                                              
                }

                #load-file-configuration-input {
                    position: absolute;                    
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                    width: 180px;
                    opacity: 0;
                }
                
                #load-file-configuration-input:hover {
                    cursor: pointer;
                }

                #load-file-configuration-text {
                    position: absolute;                                   
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                    width: inherit;
                    text-align: center; 
                }

                .load-file-configuration-button {
                    position: relative;                                        
                    border-radius: 10px;
                    background-color: white;
                    box-shadow: 0 3px 8px 0 rgba(0, 0, 0, 0.2);
                }                  
            </style>
        `;

        // Append the main section to the custom-footer.
        this.shadow.appendChild(this.mainSection);            
    }

    async loadFileConfiguration(input) {
        // Getting the service.
        const loadConfigurationService = new LoadConfigurationService();

        const file = input.files[0];
                        
        const dataJson = await new Response(file).json();
        loadConfigurationService.load(dataJson);                    
    }

    /**
     * @brief active the button of every footer items.
     */
    activeItems() {
        const items = this.shadow.querySelectorAll('.footer-item');
        
        for (const item of items) {
            this.enableItem(item);            
        }
    }

    disableItems() {
        const items = this.shadow.querySelectorAll('.footer-item');
        
        for (const item of items) {
            this.disableItem(item);            
        }        
    }

    disableItem(item) {        
        item.disabled = true;        
    }

    enableItem(item) {        
        item.disabled = false;
    }

    disableItemById(id) {
        const item = this.shadow.getElementById(id);
        item.disabled = true;        
    }

    enableItemById(id) {
        const item = this.shadow.getElementById(id);
        item.disabled = false;
    }

    changeItemState(item) {
        if (item.disabled) {
            item.disabled = false;
            item.style.cursor = 'pointer';
        } else {
            item.disabled = true;
            item.style.cursor = 'auto';
        }
    }

    /**
     * @brief return the item which has the id "id".
     * @param id the id of the items.
     * @returns the item.
     */
    getItemById(id) {
        return this.shadow.getElementById(id);
    }

    /**
     * @brief define the selected item and execute its method whenSelected().
     * @param item the item to select.
     */
    setSelectedItem(item) {
        item.whenSelected();
        this.selectedItem_ = item;
    }

    /**
     * @brief unselect the current selected item and execute its method unselected().
     */
    unselectItem() {
        this.selectedItem_.unselected();
        this.selectedItem_ = null;
    }

    /**
     * @brief return the selected item.
     * @returns the selected item.
     */
    get selectedItem() {
        return this.selectedItem_;
    }
}

// Define the custom-footer HTML element.
customElements.define('custom-footer', CustomFooter);


class CustomHeader extends HTMLElement {
    constructor() {
        super();

        this.shadow = this.attachShadow({ mode: 'open' });

        // Create main section.
        this.mainSection = document.createElement('section');
        this.mainSection.id = 'section-custom-header';
        
        this.messageSpan = document.createElement('span');
        this.messageSpan.id = 'message';
        this.messageSpan.textContent = 'Select the lower left corner of the study area!';

        this.mainSection.appendChild(this.messageSpan);
        
        // CSS of the custom-header element.
        this.shadow.innerHTML = `
            <style>    
                #message {    
                    border: 0px #A9A9A9 solid;
                    height: 20px;
                    border-radius: 10px;
                    width: 400px;
                    background-color: inherit;    
                    text-align: center;
                    font-family: Arial;
                    font-size: 13px;
                }
            </style>
        `;

        // Append the main section to the custom-header.
        this.shadow.appendChild(this.mainSection);            
    }

    setDefaultMesssage() {
        this.messageSpan.textContent = 'Choose an action!';
    }

    setMessage(message) {
        this.messageSpan.textContent = message;
    }
}

// Define the custom-header HTML element.
customElements.define('custom-header', CustomHeader);


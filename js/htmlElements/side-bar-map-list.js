/**
 *  The side bar map list of the main page.
 */
class SideBarMapList extends HTMLElement {
    constructor() {
        super();

        this.shadow = this.attachShadow({ mode: 'open' });

        this.style.display = 'block';
        
        // Creating the main section.
        this.mainSection = document.createElement('section');
        this.mainSection.id = 'section-side-bar-map-list';

        // Creating the title of the section.
        const titleDiv = document.createElement('div');
        titleDiv.textContent = 'Propagation map: ';
        
        this.mainSection.appendChild(titleDiv);

        // TODO: Some maps wasn't defined (teacher code). 
        // Creating the list of map item.
        const mapListItem = [
            {
                id: 'power-map',
                object: new PowerMapItem(),
                textContent: 'Power mapping'
            },            
            {
                id: 'diffraction-loss-map',
                object: new DiffractionLossMapItem(),
                textContent: 'Diffraction loss mapping'
            },
            // {
            //     id: 'residence-map',
            //     object: new ResidenceMapItem(),
            //     value: 'Residence time mapping'
            // },
            // {
            //     id: 'handover-map',
            //     object: new HandoverMapItem(),
            //     value: 'Handover mapping'
            // },
            {
                id: 'interference-map',
                object: new InterferenceMapItem(),
                textContent: 'C / I mapping'
            },
            {
                id: 'cell-map',
                object: new CellMapItem(),
                textContent: 'Cell mapping'
            }                        
        ];

        // Add every items to the side bar map list section.
        for (const mapItem of mapListItem) {            
            // Creating the button.
            const button = document.createElement('button');
        
            // Configuring the button.
            button.id = mapItem.id;            
            button.textContent = mapItem.textContent;
            button.disabled = true;
            button.className = 'side-bar-map-item';            
            button.style.width = '100%';                        
            
            // Event handler.
            button.addEventListener('click', () => this.setSelectedItem(mapItem.object));            

            // Append to the main section.
            this.mainSection.appendChild(button);
        }

        // Append the main section to the side bar.
        this.shadow.appendChild(this.mainSection);
    }

    /**
     * @brief active the button of every map items.
     */
    activeItems() {
        const items = this.shadow.querySelectorAll('.side-bar-map-item');
                
        for (const item of items) {
            this.enableItem(item);            
        }
    }

    disableItems() {
        const items = this.shadow.querySelectorAll('.side-bar-map-item');
                
        for (const item of items) {
            this.disableItem(item);            
        }
    }

    disableItem(item) {        
        item.disabled = true;        
    }

    enableItem(item) {        
        item.disabled = false;
    }

    disableItemById(id) {
        const item = this.shadow.getElementById(id);
        item.disabled = true;        
    }

    enableItemById(id) {
        const item = this.shadow.getElementById(id);
        item.disabled = false;
    }

    changeItemState(item) {       
        if (item.disabled) {
            item.disabled = false;        
            item.style.cursor = 'pointer';
        } else {            
            item.disabled = true;
            item.style.cursor = 'auto';
        }
    }

    /**
     * @brief return the item which has the id "id".
     * @param id the id of the items.
     * @returns the item.
     */
    getItemById(id) {
        return this.shadow.getElementById(id);
    }

    /**
     * @brief define the selected item and execute its method whenSelected().
     * @param item the item to select.
     */
    setSelectedItem(item) {
        if (this.selectedItem) {
            this.unselectItem();
        }

        item.whenSelected();
        this.selectedItem_ = item;
    }

    /**
     * @brief unselect the current selected item and execute its method unselected().
     */
    unselectItem() {
        this.selectedItem_.unselected();
        this.selectedItem_ = null;
    }

    /**
     * @brief return the selected item.
     * @returns the selected item.
     */
    get selectedItem() {
        return this.selectedItem_;
    }
}

// Define the side bar map list HTML element.
customElements.define('side-bar-map-list', SideBarMapList);


/**
 * A static class which add some methods to the given map.
 */
class MapDrawingExtension {
    constructor() {                
    };

    /**
     * Add the extensions to the map.     
     */
    static addExtensions(map) {
        map.addMarker = MapDrawingExtension.addMarker;                
        map.addMarkers = MapDrawingExtension.addMarkers;                
        map.addPolygon = MapDrawingExtension.addPolygon;         
        map.addPolyline = MapDrawingExtension.addPolyline;         
        map.addCircle = MapDrawingExtension.addCircle;         
    }

    /**
     * Append a marker on the map.
     */
    static addMarker(position, options) {
        return L.marker([position.lat, position.lng], options).addTo(this);
    }

    /**
     * Append a list of markers on the map.
     */
    static addMarkers(positions, options) {                
        for (const position of positions) {
            L.marker([position.lat, position.lng], options).addTo(this);
        }                        
    }

    static addPolygon(positions, options) {
        return L.polygon(positions, options).addTo(this);
    }

    static addPolyline(positions, options) {
        return L.polygon(positions, options).addTo(this);
    }

    static addCircle(position, options) {
        return L.circle([position.lat, position.lng], options).addTo(this);       
    }
}
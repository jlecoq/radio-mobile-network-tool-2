class ZoneDrawingExtension {
    constructor() {                
    };

    /**
     * Add the extensions to the map.     
     */
    static addExtensions(zone) {                
        zone.drawBlock = ZoneDrawingExtension.drawBlock;               
    }

    static drawBlock(blockPosition) {
        if (blockPosition.i >= this.numberOfBlocksInLat || blockPosition.j >= this.numberOfBlocksInLng) {
            console.log("You're trying to draw an inexisting block.");
            return;
        }

        const firstBinPosI = blockPosition.i * this.numberOfBinsOnBlockSide;
        const firstBinPosJ = blockPosition.j * this.numberOfBinsOnBlockSide;
            
        const options = {                        
            color : 'red',
            fillColor: '#f03',
            fillOpacity: 1,
            radius: 1
        };

        for (let i = firstBinPosI; i < firstBinPosI + this.numberOfBinsOnBlockSide; i++) {
            for (let j = firstBinPosJ; j < firstBinPosJ + this.numberOfBinsOnBlockSide; j++) {
                const point = L.latLng(this.toLat(i), this.toLng(j));                
                
                this.map.addCircle(point, options);
            }
        }
    }

}

class Location {
    constructor(
        lat,
        lng
    ) {
        this.lat = lat;
        this.lng = lng;
    }

    /**
     * Returns final coordinates (lat2, lon2) [in degrees] given initial coordinates
     * (lat1, lon1) [in degrees] and a bearing [in degrees] and distance [in m].    
     */
    destPointGivenDistanceAndBearing(distance, bearingAngle) {
        const { toDegrees, toRadians } = MathUtil;
        const { asin, sin, cos, atan2 } = Math;

        const rStartLat = toRadians(this.lat)
        const rStartLng = toRadians(this.lng)
        const rBearing = toRadians(bearingAngle) // the bearing (clockwise from north),
        const rDistance = ConvertUtil.mToKm(distance) / GeoUtil.EARTH_RADIUS // normalize linear distance to radian angle (it's the angular distance).

        const rDestLat = asin(sin(rStartLat) * cos(rDistance) + cos(rStartLat) * sin(rDistance) * cos(rBearing))

        const rDestLng =
            rStartLng +
                    atan2(
                        sin(rBearing) * sin(rDistance) * cos(rStartLat),
                        cos(rDistance) - sin(rStartLat) * sin(rDestLat)
                    );

        if (GeoUtil.isEarthCoordinate(rDestLat, rDestLng)) {
            return new Location(toDegrees(rDestLat), toDegrees(rDestLng));
        } else {
            return null;
        }
    }
}   

class LocationHelper {
    /**
     * Converts a leaflet LatLng object into a Location object.
     */
    static toLocation(locationFromLeaflet) {                
        return new Location(
            locationFromLeaflet.lat,
            locationFromLeaflet.lng
        );
    }
}
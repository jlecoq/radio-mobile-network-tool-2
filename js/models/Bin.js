class Bin {
    constructor() {
        this.bins = [];
            
        new EventService().publish(EVENT.INITIALIZED_BIN, this, true);
    }
    
    empty() {
        while (this.bins.length) {
            this.bins.pop().remove();
        }    
    }

    add(element) {
        this.bins.push(element);
    }
}

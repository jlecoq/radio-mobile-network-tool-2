/**
 * An obstacle which diffract the transmitted signal of a base station. It blocks the line of sight.
 */
class Obstacle extends Thing {
    constructor(location, height) {
        super(location, height);
    }
}
/**
 * Base class to creates objects.
 */
class Thing {
    constructor(location, height) {
        this.location = location; 
        this.height = height;
    }
}
class Building {
    constructor() {
        this.cornersLocation = []; // Store the location of every corners.
        this.height = 0; 
    }
}

// Static property.
Building.buildingInConstruction = null;
Building.buildingPolygons = [];

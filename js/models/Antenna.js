/**
 * An antenna/base station.
 */
class Antenna extends Thing {
    constructor(object) {                
        super(object.location, object.height);        

        this.isOmni = object.omni; // Omni or not
        this.azimut = object.azimut; // Azimuth in radian
        this.power = object.radiatedPower;	// Power in Db
        this.tilt = object.tilt; // Tilt in radian
        // this.transmissionFrequency = object.transmissionFrequency; // Transmission frequency
        // this.wavelength = GeoUtil.CELERITY / (this.transmissionFrequency * 10 ** 6);
        this.coverage = 32_000; // Transmission frequency        
        this.SFH = object.SFH;
        this.lost = object.lost;
        this.EIRP = object.EIRP;
        this.name = object.name;
        this.type = object.specificType;
        this.BCCH = object.BCCH;
        this.gain = object.gain;
        this.frequencies = object.frequencies;
        this.HSN = object.HSN;

        Antenna.antennas.push(this);
        Antenna.nbAntennas++;
    }
}

// Static properties: Static properties using: "static markerAntennas = [];" syntax doesn't work on safari.
Antenna.markerAntennas = [];
Antenna.nbAntennas = 0;
Antenna.antennas = [];

class Map {
	constructor() {
		const options = {
			subscribeOnce: true
		}

		this.eventService = new EventService()
		this.eventService.subscribe(EVENT.PLACED_ZONE, initizalizedZone => this.zone = initizalizedZone, options)

		this.setupMap()
		this.handleMapEvent()
		this.hideFrameAntenna()

		this.isDoubleClickCalled_ = 0
	}

	setupMap() {
		const utbmBelfort = new Location(47.641330, 6.844349)
		const zoomLevel = 14

		const mapCanvas = document.getElementById('map-canvas')
		mapCanvas.style.height = '100%'

		this.map = L.map('map-canvas').setView([utbmBelfort.lat, utbmBelfort.lng], zoomLevel)
		this.map.doubleClickZoom.disable()

		const tileLayer = `https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=${MAPBOX.ACCESS_TOKEN}`

		L.tileLayer(tileLayer, {
			maxZoom: 18,
			attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
				'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			id: 'mapbox.streets'
		}).addTo(this.map)

		// Add some drawing extensions to the map, to make the code shorter.
		MapDrawingExtension.addExtensions(this.map)

		this.eventService.publish(EVENT.INITIALIZED_MAP, this.map, true)
	}

	handleMapEvent() {
		this.map.on('click', (event) => this.onMapClick(event))
		this.map.on('dblclick', (event) => this.onMapDbClick(event))
	}

	hideFrameAntenna() {
		// TODO: Refactor!!
		const frameAntenna = document.getElementById('frameAntenna')
		frameAntenna.style.visibility = 'hidden'
	}

	async onMapClick(event) {
		const isDoubleClick = await this.isDoubleClick()

		if (isDoubleClick) {
			return
		}

		// Fetch the location.
		const location = event.latlng

		// If the zone isn't placed, we place it.
		if (!this.zone) {
			new ZoneItem().placeZone(location)
			return
		}

		// Fetch the selected item.
		const customFooter = document.getElementById('footer')
		const selectedItem = customFooter.selectedItem

		// Place the selected item.
		if (selectedItem) {
			// If it's a building we don't unselect it.
			if (TypeUtil.typeof(selectedItem) === 'BuildingItem') {
				selectedItem.placeItem(location)
				return
			}

			selectedItem.placeItem(location)
			customFooter.unselectItem()
		}
	}

	onMapDbClick(event) {
		// Fetch the location.
		const location = event.latlng

		// If the zone isn't placed, we place it.
		if (!this.zone) {
			new ZoneItem().placeZone(location)
			return
		}

		// Fetch the selected item.
		const customFooter = document.getElementById('footer')
		const selectedItem = customFooter.selectedItem

		// Place the selected item.
		if (selectedItem) {
			const isPlaced = selectedItem.placeItem2(location)

			// If the item has been placed successfully.
			if (isPlaced === true) {
				customFooter.unselectItem()
			}
		}
	}

	async isDoubleClick() {
		this.isDoubleClickCalled_++

		// The function has been called two times in a row (< 200ms). Yes, it's a double click.
		if (this.isDoubleClickCalled_ > 1) {
			return true
		}

		// Wait 200 ms.
		await TimerUtil.timeout(200)

		// Yes, it's a double click.
		if (this.isDoubleClickCalled_ > 1) {
			this.isDoubleClickCalled_ = 0
			return true
		}

		this.isDoubleClickCalled_ = 0
		return false
	}
}

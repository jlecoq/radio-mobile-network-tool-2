/**
 * The study zone used to test our propagation models.
 * lat correspond to i, lng correspond to j.
 */
class Zone {
    constructor(location, numberOfBlocksInLat, numberOfBlocksInLng) {
        this.southWestCorner = location; // The south-West side of the deployment area.
        this.bloc = location; // Current scanned block.

        this.relief = []; // Elevation of the natural landscape.
        this.architecture = []; // Elevation of the buildings.
        this.elevation = []; // The total elevation of natural landscape and buildings.
        
        // this.isOnRoad = []; // TODO: Not used, implement it.
        // this.cellCoverage = []; // TODO: Not used, implement it.
        // this.orientation = []; // TODO: Not used, implement it.

        this.numberOfBlocksInLng = numberOfBlocksInLng; // Number of blocks on the lng axis (e.i j axis, x axis).
        this.numberOfBlocksInLat = numberOfBlocksInLat; // Number of blocks on the lat axis (e.i i axis, y axis).
        this.numberOfBlock = this.numberOfBlocksInLng * this.numberOfBlocksInLat; // Total number of blocks.

        this.numberOfBinsOnBlockSide = 20; // Number of bins on the side of a block.
        this.numberOfBinsInLng = this.numberOfBinsOnBlockSide * this.numberOfBlocksInLng; // Number of bins on the lng axis (e.i j axis, x axis).
        this.numberOfBinsInLat = this.numberOfBinsOnBlockSide * this.numberOfBlocksInLat; // Number of bins on the lat axis (e.i i axis, y axis).
        this.numberOfBins = this.numberOfBinsInLng * this.numberOfBinsInLat; // Total number of bins.

        // Initializing latBin, lngBin, latBloc, lngBloc.

        const point1 = new Location(this.southWestCorner.lat + 0.001, this.southWestCorner.lng);
        const distance1 = GeoUtil.distance(this.southWestCorner, point1);

        const point2 = new Location(this.southWestCorner.lat, this.southWestCorner.lng + 0.001);
        const distance2 = GeoUtil.distance(this.southWestCorner, point2);

        // Lat height of a bin (about 10 meters).
        // When adding this value to a latitude, you got a new latitude which belongs to the next bin in lat axis (e.i j axis, x axis).
        this.latBin = 0.001 * 10 / distance1; 

        // Lng width of a bin (about 10 meters).
        // When adding this value to a longitude, you got a new longitude which belongs to the next bin in lng axis (e.i j axis, x axis).
        this.lngBin = 0.001 * 10 / distance2;

        // Lat height of a block (about 200 meters). 
        // When adding this value to a latitude, you got a new latitude which belongs to the next block in lat axis (e.i i axis, y axis).
        this.latBloc = this.latBin * this.numberOfBinsOnBlockSide; // The same for the latitude

        // Lng width of a block (about 200 meters). 
        // When adding this value to a longitude, you got a new longitude which belongs to the next block in lng axis (e.i j axis, x axis).
        this.lngBloc = this.lngBin * this.numberOfBinsOnBlockSide; 
        
        // Initializing binHeight, binWidth.
        const point3 = new Location(this.southWestCorner.lat + this.latBloc, this.southWestCorner.lng);        
        const point4 = new Location(this.southWestCorner.lat, this.southWestCorner.lng + this.lngBloc);        

        this.binHeight = GeoUtil.distance(this.southWestCorner, point3) / this.numberOfBinsOnBlockSide;
        this.binWidth = GeoUtil.distance(this.southWestCorner, point4) / this.numberOfBinsOnBlockSide;      
        
        ZoneDrawingExtension.addExtensions(this);
        
        const eventService = new EventService();

        const options = {
            subscribeOnce: true
        };
                
        eventService.subscribe(EVENT.INITIALIZED_MAP, initizalizedMap => this.map = initizalizedMap, options);
        eventService.publish(EVENT.PLACED_ZONE, this, true);        
    }

    toLat(i) {
        return this.southWestCorner.lat + i * this.latBin;
    }

    toLng(j) {
        return this.southWestCorner.lng + j * this.lngBin;
    }

    toI(lat) {
        return Math.round((lat - this.southWestCorner.lat) / this.latBin); // TODO: Math.floor or Math.round
    }

    toJ(lng) {
        return Math.round((lng - this.southWestCorner.lng) / this.lngBin); // TODO: Math.floor or Math.round
    }

    updateIJ(point, latlng) {
        point.i = this.toI(latlng.lat);
        point.j = this.toJ(latlng.lng);
    }

    setLatLng(point, i, j) {
        point.lat = this.toLat(i);
        point.lng = this.toLng(j);            
    }

    setIJ(point) {                
        point.i = this.toI(point.lat);
        point.j = this.toJ(point.lng);
    }
}

// Static property.
Zone.group1 = L.featureGroup();
Zone.bl = 0; // Counter of block line (during scanning)
Zone.bc = 0; // Counter of block column (during scanning)
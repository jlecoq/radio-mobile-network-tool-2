class ColorUtil {
    /**
     * Returns a color depending of the signal power received at a given position.
     */
    static GetColor(power) {
        let r, g, b;

        console.log(power);
        if (power > 0) {
            r = 251;
            g = 0;
            b = 0;
        } else if (power > -50) {
            r = 250;
            g = 123;
            b = 0;
        } else if (power > -70) {
            r = 251;
            g = 220;
            b = 0;
        } else if (power > -80) {
            r = 244;
            g = 251;
            b = 0;
        } else if (power > -90) {
            r = 188;
            g = 251;
            b = 0;
        } else if (power > -100) {
            r = 1;
            g = 250;
            b = 57;
        } else if (power > -105) {
            r = 0;
            g = 251;
            b = 201;
        } else if (power > -110) {
            r = 0;
            g = 207;
            b = 251;
        } else if (power > -115) {
            r = 1;
            g = 69;
            b = 250;
        } else {
            r = 51;
            g = 1;
            b = 250;
        }

        let strRed, strGreen, strBlue;

        if (r < 16) {
            strRed = "0" + r.toString(16);
        } else {
            strRed = r.toString(16);
        }

        if (g < 16) {
            strGreen = "0" + g.toString(16);
        } else {
            strGreen = g.toString(16);
        }

        if (b < 16) {
            strBlue = "0" + b.toString(16);
        } else {
            strBlue = b.toString(16);
        }
        
        return "#" + strRed + strGreen + strBlue;
    }

    static hslToRgb(h, s, v) {
        /**
         * I: An array of three elements hue (h) ∈ [0, 360], and saturation (s) and value (v) which are ∈ [0, 1]
         * O: An array of red (r), green (g), blue (b), all ∈ [0, 255]
         * Derived from https://en.wikipedia.org/wiki/HSL_and_HSV
         * This stackexchange was the clearest derivation I found to reimplement https://cs.stackexchange.com/questions/64549/convert-hsv-to-rgb-colors
         */

        const hprime = h / 60;
        const c = v * s;
        const x = c * (1 - Math.abs(hprime % 2 - 1));
        const m = v - c;
        let r, g, b;
        
        if (!hprime) {
            r = 0;
            g = 0;
            b = 0;
        }
        if (hprime >= 0 && hprime < 1) {
            r = c;
            g = x;
            b = 0
        }
        if (hprime >= 1 && hprime < 2) {
            r = x;
            g = c;
            b = 0
        }
        if (hprime >= 2 && hprime < 3) {
            r = 0;
            g = c;
            b = x
        }
        if (hprime >= 3 && hprime < 4) {
            r = 0;
            g = x;
            b = c
        }
        if (hprime >= 4 && hprime < 5) {
            r = x;
            g = 0;
            b = c
        }
        if (hprime >= 5 && hprime < 6) {
            r = c;
            g = 0;
            b = x
        }

        r = Math.round((r + m) * 255);
        g = Math.round((g + m) * 255);
        b = Math.round((b + m) * 255);

        return [r, g, b]
    }

    static diffractionLossColor(loss) {        
        loss = Math.abs(loss);            
        const max_power_value = 200;
        const min_power_value = 0;
        const max_hue = 255;

        const normal = Math.round(((loss - max_power_value) / (min_power_value - max_power_value)) * max_hue);
        const [r, g, b] = ColorUtil.hslToRgb(normal, 1, 1);

        return ColorUtil.rgbToHex(r, g, b);
    }

    static powerReceivedColor(rssi) {
        rssi = Math.abs(rssi);
        const max_power_value = 115;
        const min_power_value = 0;
        const max_hue = 255;

        const normal = Math.round(((rssi - max_power_value) / (min_power_value - max_power_value)) * max_hue);
        const [r, g, b] = ColorUtil.hslToRgb(normal, 1, 1);

        return ColorUtil.rgbToHex(r, g, b);
    }

    static componentToHex(c) {
        const hex = c.toString(16);
        return hex.length === 1 ? "0" + hex : hex;
    }

    static rgbToHex(r, g, b) {
        return "#" + ColorUtil.componentToHex(r) + ColorUtil.componentToHex(g) + ColorUtil.componentToHex(b);
    }
}
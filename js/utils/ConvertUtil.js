class ConvertUtil {
    /**
     * Convert some kilometers into meters.
     */
    static kmToM(km) {
        return km * 1000;
    } 
        
    /**
     * Convert some meters into kilometers.
     */
    static mToKm(m) { 
        return m / 1000;
    } 
}
class MathUtil {
    static toRadians(degrees) {
        return degrees * (Math.PI / 180);
    }

    static toDegrees(radians) {
        return radians * (180 / Math.PI);
    }
    
    /**
    * min = 0 (included)
    * max (excluded)
    * Integer
    * [min, max[
    */
    static randomIntFrom0To(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }  
}
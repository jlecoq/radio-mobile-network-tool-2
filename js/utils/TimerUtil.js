class TimerUtil {

    /**
     * Creates a timeout and resolve a promise when the timeout finish.
     */
    static timeout(time) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {        
                resolve();
            }, time);
        });
    }    
}
class GeoUtil {

    /**
     * Retuns the distance between two geographical points 
     * using spherical law of cosines approximation (in meters).
     */
    static distance(startPosition, endPosition) {
        const { sin, cos, atan2, sqrt } = Math;   
        const { toRadians } = MathUtil; 

        // Get the latitude in radians.
        const startLatitude = toRadians(startPosition.lat);
        const endLatitude = toRadians(endPosition.lat);

        const sinDLat = sin(toRadians(endPosition.lat - startPosition.lat) / 2);        
        const sinDLng = sin(toRadians(endPosition.lng - startPosition.lng) / 2);        
                
        // Get the alpha.
        const alpha = sinDLat * sinDLat + cos(startLatitude) * cos(endLatitude) * sinDLng * sinDLng;

        const c = 2 * atan2(sqrt(alpha), sqrt(1 - alpha));
                        
        // Return the distance.        
        return ConvertUtil.kmToM(GeoUtil.EARTH_RADIUS * c);
    }

    /**
     * Checks if a given coordinates are valid earth coordinates.     
     */
    static isEarthCoordinate(lat, lng) {        
        return lat < 90 && lat > -90 && lng > -180 && lng < 180;
    }

    /**
     * Return the bearing angle between two coordinates.
     * Reference north south line.
     * From 0 to 180 => north to south.
     * From 0 to -180 => north to south.
     * 0 is north, 180 and -180 are south.
     */
    static initialBearing(originCoordinate, destCoordinate) {
        const { sin, cos, atan2 } = Math;
        const { toRadians, toDegrees } = MathUtil;

        const rStartLat = toRadians(originCoordinate.lat);
        const rStartLng = toRadians(originCoordinate.lng);
        const rEndLat = toRadians(destCoordinate.lat);
        const rEndLng = toRadians(destCoordinate.lng);

        const y = sin(rEndLng - rStartLng) * cos(rEndLat);
        const x = (cos(rStartLat) * sin(rEndLat)
                - sin(rStartLat) * cos(rEndLat)
                * cos(rEndLng - rStartLng));

        return toDegrees(atan2(y, x));
    }

    /**
     * Return the initial bearing angle between two coordinates.
     * Reference north south line. Clock wise from the north.
     * From 0 to 360.
     * 0 is north, 180 is south and 360 is north.
     */
    static compassInitialBearing(originCoordinate, destCoordinate) {
        return (GeoUtil.initialBearing(originCoordinate, destCoordinate) + 360) % 360;
    }
    
    /**
     * Return a list of points between two coordinates. The number of points will depends of the step (in meters) 
     * between every points.
     */
    static pointsFromStraightLine(coordinate1, coordinate2, options) {
        // Defining the options.
        options = {
            step: 30,
            includeFirst: true,
            includeLast: true,
            ...options
        };

        // Checking the validity of the step.
        if (options.step <= 0) {
            throw new Error('The step should be an unsigned integer greater than 0.');
        }

        // Defining some variables.
        const angle = GeoUtil.compassInitialBearing(coordinate1, coordinate2);
        const points = []; 
        let point = coordinate1;

        // Including the first point.
        if (options.includeFirst) {
            points.push(coordinate1);
        }
        
        // Creating a point while it's possible.
        while (options.step < GeoUtil.distance(point, coordinate2)) {
            point = point.destPointGivenDistanceAndBearing(options.step, angle);
            points.push(point);         
        }

        // Including the last point.
        if (options.includeLast) {
            points.push(coordinate2);
        }
        
        return points;
    }

    /**
     * Checks if a given location is inside a polygon.
     */
    static isPointInsidePolygon(location, polygon) {
        let inside = false;
        const x = location.lat, y = location.lng;

        for (let ii = 0; ii < polygon.getLatLngs().length; ii++){
            let polyPoints = polygon.getLatLngs()[ii];
        
            for (let i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) {
                const xi = polyPoints[i].lat, yi = polyPoints[i].lng;
                const xj = polyPoints[j].lat, yj = polyPoints[j].lng;
    
                let intersect = ((yi > y) !== (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                
                if (intersect) {
                    inside = !inside;
                }
            }
        }
    
        return inside;
    }
}

// Static properties.

// Mean Earth Radius, as recommended for use by
// the International Union of Geodesy and Geophysics,
GeoUtil.EARTH_RADIUS = 6_371;
GeoUtil.CELERITY = 3.0 * 10**8;


// Old way to calculate distances
// /**
//  * Retuns the distance between two position in meters.
//  */
// static distance(startPosition, endPosition) {
//     const { sin, cos, acos } = Math;   
//     const { toRadians,  } = MathUtil; 

//     // Get the latitude in radians.
//     const startLatitude = toRadians(startPosition.lat);
//     const endLatitude = toRadians(endPosition.lat);

//     // Get the difference of longitudes in radians.
//     const deltaLongitude = toRadians(startPosition.lng - endPosition.lng);        
            
//     // Get the alpha.
//     const alpha = sin(startLatitude) * sin(endLatitude) + cos(startLatitude) * cos(endLatitude) * cos(deltaLongitude);                    
                    
//     // Return the distance.        
//     return ConvertUtil.kmToM(GeoUtil.EARTH_RADIUS * acos(alpha));
// }


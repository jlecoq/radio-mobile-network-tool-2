class PropagationUtil {

    /**
     * Returns the diffraction loss due to the presence of an obstacle which broke the LOS (line of sight) between 
     * a base station and a mobile station.
     */
    static diffractionLoss(Loss, h, wavelength, d1, d2) {
        const { log10, exp, sqrt, pow } = Math;
                                
        const v = h * sqrt((2 * (d1 + d2)) / (wavelength * d1 * d2));

        if (v <= -1) {
            Loss += 0;
        } else if (-1 < v && v <= 0) {
            Loss += 20 * log10(0.5 - 0.62 * v);
        } else if (0 < v && v <= 1) {
            Loss += 20 * log10(0.5 * exp(-0.95 * v));
        } else if (1 < v && v <= 2.4) {
            Loss += 20 * log10(0.4 - sqrt(0.1184 - pow(-0.1 * v + 0.38, 2)));
        } else if (2.4 < v) {
            Loss += 20 * log10(0.255 / v);
        }

        return Loss;        
    }
}
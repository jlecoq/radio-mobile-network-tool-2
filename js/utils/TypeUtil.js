class TypeUtil {
    static typeof(object) {
        return object.constructor.name;
    }
}